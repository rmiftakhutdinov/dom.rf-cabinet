package data;

public class CommonData {

    //логин и пароль от ЛК - rm@33slona.ru
    public String userEmail = "rm@cometrica.ru";
    public String userPhoneCab = "89272401145";
    public String userPass = "test2015";

    //логин и пароль от ЛК - rm+22@slona.ru
    public String secondUserEmail = "rm+22@cometrica.ru";
    public String secondUserPass = "123456";

    //логин и пароль от ЛК - rm+33@slona.ru
    public String thirdUserEmail = "rm+33@cometrica.ru";
    public String thirdUserPass = "123456";

    //логин и пароль от ЛК - rm+44@slona.ru
    public String fourthUserEmail = "rm+44@cometrica.ru";
    public String fourthUserPass = "123456";

    //логин и пароль от Админки - admin@example.com
    //public String adminUserEmail = "admin@example.com";
    public String adminUserEmail = "71118862649";
    public String adminUserPass = "test1234";

}