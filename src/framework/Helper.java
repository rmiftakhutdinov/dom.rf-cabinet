package framework;

import data.CommonData;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.ConsumptionHistoryPageCabinet;
import pageObjects.RentObjectsPageAdmin;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.util.Objects;
import java.util.Random;

public class Helper {

    // Вызов драйвера
    protected WebDriver driver;

    // Выбор драйвера и браузера
    protected WebDriver getDriver(String os){

        if (Objects.equals(os.toLowerCase(), "windows")){

            System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");

        } else {

            System.setProperty("webdriver.chrome.driver", "drivers/chrome/chromedriver");

        }

        return new ChromeDriver();

    }

    protected FirefoxDriver getFirefoxDriver (String os) {

        if (Objects.equals(os.toLowerCase(), "windows")){

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");

        } else {

            System.setProperty("webdriver.gecko.driver", "drivers/geckodriver");

        }

        // Запуск тестов без открытия браузера
        /*ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");
        return new ChromeDriver(options);*/

        return new FirefoxDriver();

    }

    // Сообщение об ошибке
    protected void afterExeptionMessage(String message){

        throw new RuntimeException("\n" + message);

    }

    // Таймаут по заданному времени
    protected void sleep(int time) throws InterruptedException {

        Thread.sleep(time);

    }

    // Клик на элемент
    protected void click(By by){

        new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента и клик по нему
    protected void click(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.elementToBeClickable(by)).click();

    }

    // Ожидание элемента с выбором времени
    protected void waitBy(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Ожидание элемента с установленным по дефолту временем
    protected void waitBy(By by){

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(by));

    }

    // Поиск и ожидание элемента
    protected WebElement findElement(By by){

        try {
            new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(by));
        } catch (java.util.NoSuchElementException | TimeoutException ex){
            throw new RuntimeException("Элемент " + by + " не найден на странице");
        }

        return driver.findElement(by);

    }

    // Авторизация в ЛК: user rm@cometrica.ru
    public void authorization() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        findElement(auth.userName).clear();
        findElement(auth.userName).sendKeys(user.userEmail);
        findElement(auth.userPassword).clear();
        findElement(auth.userPassword).sendKeys(user.userPass);

        sleep(1000);
        click(auth.submit);
        waitBy(auth.cabinetElement, 60);

    }

    // Авторизация в ЛК: user rm+22@33slona.ru
    public void authorizationSecond() throws InterruptedException {

        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        findElement(auth2.userName).clear();
        findElement(auth2.userName).sendKeys(user.secondUserEmail);
        findElement(auth2.userPassword).clear();
        findElement(auth2.userPassword).sendKeys(user.secondUserPass);

        sleep(1000);
        click(auth2.submit);
        waitBy(auth2.cabinetElement, 60);

    }

    // Авторизация в ЛК: user rm+33@33slona.ru
    public void authorizationThird() throws InterruptedException {

        AuthorizationPageCabinet auth3 = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        findElement(auth3.userName).clear();
        findElement(auth3.userName).sendKeys(user.thirdUserEmail);
        findElement(auth3.userPassword).clear();
        findElement(auth3.userPassword).sendKeys(user.thirdUserPass);

        sleep(1000);
        click(auth3.submit);
        waitBy(auth3.cabinetElement, 60);

    }

    // Авторизация в ЛК: user rm+44@33slona.ru
    public void authorizationFourth() throws InterruptedException {

        AuthorizationPageCabinet auth3 = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        findElement(auth3.userName).clear();
        findElement(auth3.userName).sendKeys(user.fourthUserEmail);
        findElement(auth3.userPassword).clear();
        findElement(auth3.userPassword).sendKeys(user.fourthUserPass);

        sleep(1000);
        click(auth3.submit);
        waitBy(auth3.cabinetElement, 60);

    }

    // Авторизация в Админке: user admin@example.com
    public void authorizationInAdmin() throws InterruptedException {

        RentObjectsPageAdmin authAdmin = new RentObjectsPageAdmin();
        CommonData user = new CommonData();

        findElement(authAdmin.userName).clear();
        findElement(authAdmin.userName).sendKeys(user.adminUserEmail);
        findElement(authAdmin.userPassword).clear();
        findElement(authAdmin.userPassword).sendKeys(user.adminUserPass);

        sleep(1000);
        click(authAdmin.submit);
        waitBy(authAdmin.filter, 60);

    }

    // Добавление файла
    protected void setClipboardData(String string) {

        StringSelection stringSelection = new StringSelection(string);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);

    }

    // Скролл вниз (хз как работает)
    protected void scrollDown() {

        ((JavascriptExecutor)driver).executeScript("window.scroll(0, window.innerHeight);");

    }

    // Скролл к элементу
    protected void scrollToElement(By by) throws InterruptedException {

        WebElement element = findElement(by);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        sleep(1000);

    }

    // Поиск элемента на странице
    protected boolean isElementPresent(By selector){

        try{
            driver.findElement(selector); // не убирать 'driver'
            return true;
        } catch (NoSuchElementException | TimeoutException ex){
            return false;
        }

    }

    // Сендкейс - вставить текст
    protected void sendKeys(By by, String string){

        findElement(by).clear();
        findElement(by).sendKeys(string);

    }

    // Сендкейс - нажать с клавиатуры
    protected void sendKeys(By by, Keys keys){

        findElement(by).clear();
        findElement(by).sendKeys(keys);

    }

    // Ожидание исчезновения элемента по локатору
    protected boolean waitForElementIsNotPresent(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.invisibilityOfElementLocated(by));
        return false;

    }

    // Ожидание исчезновения элемента по локатору
    protected boolean waitForElementIsNotPresent(By by){

        new WebDriverWait(driver, 10).until(ExpectedConditions.invisibilityOfElementLocated((by)));
        return false;

    }

    // Ожидание отсутствия элемента на странице
    protected boolean elementIsNotPresent(By selector){

        try{

            int timeouts = 5;
            new WebDriverWait(driver, timeouts).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }

    // Ожидание исчезновения видимости элемента
    protected boolean elementIsNotVisible (By selector){

        try{
            new WebDriverWait(driver, 5).until(ExpectedConditions.invisibilityOf(findElement(selector)));
            return true;
        } catch(TimeoutException ex){
            return false;
        }

    }

    // Ожидание отсутствия элемента на странице
    protected boolean elementIsNotPresent(By selector, int time){

        try{

            new WebDriverWait(driver, time).until(ExpectedConditions.presenceOfElementLocated(selector));
            return false;

        } catch (TimeoutException ex){

            return true;

        }

    }

    // Выбор селекта по value
    protected void selectByValue(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByValue(string);

    }

    // Выбор селекта по тексту
    protected void selectByText(By by, String string){

        Select select = new Select(findElement(by));
        select.selectByVisibleText(string);

    }

    // Генерация случайных символов
    protected String randomString(String data, int length){

        /**
         * @data - принимает на вход символьный набор, из которого генерится строка
         * @length - длина выходного значения
         */

        StringBuffer string = new StringBuffer();
        for (int i=0; i < length; i++){

            string.append(data.charAt(new Random().nextInt(data.length())));

        }

        return string.toString();

    }

    // Ожидание исчезновения элемента из дома
    protected void domElementIsNotVisible(By by, int time){

        new WebDriverWait(driver, time).until(ExpectedConditions.stalenessOf(findElement(by)));

    }

    // Ожидание отрезка URL страницы
    protected boolean urlContains(String phrase){

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(phrase));
            return true;
        } catch (TimeoutException ex){
            return false;
        }

    }

    protected void consumptionHistoryTestStand(){

        ConsumptionHistoryPageCabinet page = new ConsumptionHistoryPageCabinet();

        String coldWater = findElement(page.coldWater).getText();
        click(page.coldWater);
        String coldWaterElement = findElement(By.xpath("//a[2][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(coldWater.contains(coldWaterElement));
        waitBy(By.xpath("//p[contains(text(),'Декабрь 2018')]"));
        waitBy(By.xpath("//label[contains(text(),'10.836 м³')]"));
        waitBy(By.xpath("//p[contains(text(),'412,42')]"));

        WebElement elementColdWater = findElement(By.xpath("//div[1]/div[@class='bar cold_water']"));
        Actions action1 = new Actions(driver);
        action1.moveToElement(elementColdWater).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Декабрь 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='10.836 м³']")).isDisplayed());

        String hotWater = findElement(page.hotWater).getText();
        click(page.hotWater);
        String hotWaterElement = findElement(By.xpath("//a[3][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(hotWater.contains(hotWaterElement));
        waitBy(By.xpath("//p[contains(text(),'Декабрь 2018')]"));
        waitBy(By.xpath("//label[contains(text(),'10.667 м³')]"));
        waitBy(By.xpath("//p[contains(text(),'1 656,59')]"));

        WebElement elementHotWater = findElement(By.xpath("//div[1]/div[@class='bar hot_water']"));
        Actions action2 = new Actions(driver);
        action2.moveToElement(elementHotWater).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Декабрь 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='10.667 м³']")).isDisplayed());

        String electric = findElement(page.electric).getText();
        click(page.electric);
        String electricElement = findElement(By.xpath("//a[1][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(electric.contains(electricElement));
        waitBy(By.xpath("//p[contains(text(),'Декабрь 2018')]"));
        waitBy(By.xpath("//label[contains(text(),'154.6 кВт·ч')]"));
        waitBy(By.xpath("//p[contains(text(),'805,47')]"));

        WebElement elementElectric = findElement(By.xpath("//div[1]/div[@class='bar electricity']"));
        Actions action3 = new Actions(driver);
        action3.moveToElement(elementElectric).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Декабрь 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='154.6 кВт·ч']")).isDisplayed());

    }

    protected void consumptionHistoryDevStand(){

        ConsumptionHistoryPageCabinet page = new ConsumptionHistoryPageCabinet();

        String coldWater = findElement(page.coldWater).getText();
        click(page.coldWater);
        String coldWaterElement = findElement(By.xpath("//a[2][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(coldWater.contains(coldWaterElement));
        waitBy(By.xpath("//p[text()='Июль 2018']"));
        waitBy(By.xpath("//label[text()='1 м³']"));
        waitBy(By.xpath("//p[contains(text(),'35,40')]"));

        WebElement elementColdWater = findElement(By.xpath("//div[@class='bar cold_water']"));
        Actions action1 = new Actions(driver);
        action1.moveToElement(elementColdWater).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Июль 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='1 м³']")).isDisplayed());

        String hotWater = findElement(page.hotWater).getText();
        click(page.hotWater);
        String hotWaterElement = findElement(By.xpath("//a[3][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(hotWater.contains(hotWaterElement));
        waitBy(By.xpath("//p[text()='Июль 2018']"));
        waitBy(By.xpath("//label[text()='1 м³']"));
        waitBy(By.xpath("//p[contains(text(),'148,49')]"));

        WebElement elementHotWater = findElement(By.xpath("//div[@class='bar hot_water']"));
        Actions action2 = new Actions(driver);
        action2.moveToElement(elementHotWater).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Июль 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='1 м³']")).isDisplayed());

        String electric = findElement(page.electric).getText();
        click(page.electric);
        String electricElement = findElement(By.xpath("//a[1][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(electric.contains(electricElement));
        waitBy(By.xpath("//p[text()='Июль 2018']"));
        waitBy(By.xpath("//label[text()='1 кВт·ч']"));
        waitBy(By.xpath("//p[contains(text(),'5,13')]"));

        WebElement elementElectric = findElement(By.xpath("//div[@class='bar electricity']"));
        Actions action3 = new Actions(driver);
        action3.moveToElement(elementElectric).perform();
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='Июль 2018']")).isDisplayed());
        Assert.assertTrue(findElement(By.xpath("//div[@class='ng-binding'][text()='1 кВт·ч']")).isDisplayed());

    }

}
