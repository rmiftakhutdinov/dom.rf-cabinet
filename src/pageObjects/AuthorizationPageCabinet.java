package pageObjects;

import org.openqa.selenium.By;

public class AuthorizationPageCabinet {

    //public String pageURL = "https://dev.stage.domrf.orbita.center/renter";
    public String pageURL = "https://dev.domrf.cometrica.ru/renter";

    public By userName = By.xpath("//input[@name='username']");

    public By userPassword = By.xpath("//input[@name='password']");

    public By submit = By.xpath("//button");

    public By homePageElement = By.xpath("//div[@class='title ng-binding'][text()='Лайнер']");

    public By authPageElement = By.xpath("//h1[@class='auth-page-caption ng-binding'][text()='Вход в личный кабинет жильца']");

    public By logo = By.xpath("//a[@class='auth-page-logo logo']");

    public By cabinetElement = By.xpath("//div[@class='lk-header_setting icon-settings']");

    public By changePassword = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Забыли пароль?']");

    public By backToAuthPage = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Войти с паролем']");

    public By nextPage = By.xpath("//button[text()='Далее']");

    public By sendCodPage = By.xpath("//label[text()='Код подтверждения']");

    public By sendCodAgain = By.xpath("//button[text()='Запросить код еще раз']");

    public By changePhoneNumber = By.xpath("//a[@class='link--green link--border change-password_link ng-binding'][text()='Изменить номер']");

    public By sendLinkAgain = By.xpath("//button[@button-spinner='checkingUserName'][text()='Отправить еще раз']");

    public By input = By.xpath("//input");

    public By textRequestCodAgain = By.xpath("//span[contains(text(),'Запросить код еще раз можно')]");

    public By spinner = By.xpath("//div[@class='button-spinner-container']");

    public By eye = By.xpath("//div[@class='s-ico s-ico-eye input-eye']");

    public By termsOfUse = By.xpath("//a[text()='Условиями использования']");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By apart220 = By.xpath("//h1[@class='lk-cover_title ng-binding'][contains(text(),'220')]");

}
