package pageObjects;

import org.openqa.selenium.By;

public class ConditionsAndDocumentsPageCabinet {

    public By conditionsAndContract = By.xpath("//p[contains(text(),'Условия')]");

    public By conditionsAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By rulesOfResidence = By.xpath("//a[@class='link--green docs_link docs_link--rules ng-binding ng-scope'][text()='Правила проживания']");

    public By cancelDeparture = By.xpath("//a[text()='Отменить выезд']");

    public By cancelTerminated = By.xpath("//a[text()='Отменить расторжение']");

    public By confirm = By.xpath("//button");

    public By submit = By.xpath("//a[text()='Отправить']");

    public By close = By.xpath("//img[@alt='Закрыть']");

    public By informDateDeparture = By.xpath("//button[text()='Cообщить дату выезда']");

    public By terminatedContractOfRent = By.xpath("//button[text()='Расторгнуть договор аренды']");

    public By dateDeparture = By.xpath("//h1[contains(text(),'Дата выезда')]");

    public By dateTerminatedContract = By.xpath("//h1[contains(text(),'Дата расторжения договора')]");

    public By inputDate = By.xpath("//ul[@class='select-search-list']/li[1]");

    public By choiseDate = By.xpath("//ul[@class='select-dropdown-optgroup ng-scope']/li[4]");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By contractOfRent = By.xpath("//a[contains(text(),'Договор аренды до')]");

    public By contractOfRentElement = By.xpath("//h1[contains(text(),'Договор аренды до')]");

    public By breadcrumbs = By.xpath("//docs-galery/a[text()='Условия и договор']");

    public By img = By.xpath("//figure[1]/a/img");

    public By pdf = By.xpath("//a/span");

    public By openJPG = By.xpath("//img[@class='pswp__img'][contains(@src,'jpeg')]");

    public By openPNG = By.xpath("//img[@class='pswp__img'][contains(@src,'png')]");

    public By nextImg = By.xpath("//button[@class='pswp__button pswp__button--arrow--right']");

    public By prevImg = By.xpath("//button[@class='pswp__button pswp__button--arrow--left']");

    public By closeImg = By.xpath("//button[@class='pswp__button pswp__button--close']");

}