package pageObjects;

import org.openqa.selenium.By;

public class ConsumptionHistoryPageCabinet {

    public By consumptionHistory = By.xpath("//div[contains(@ui-sref,'consHistory')]/p");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By electric = By.xpath("//a[1][@class='payments_filter-item ng-binding ng-scope']");

    public By coldWater = By.xpath("//a[2][@class='payments_filter-item ng-binding ng-scope']");

    public By hotWater = By.xpath("//a[3][@class='payments_filter-item ng-binding ng-scope']");

    public By apart222 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'222')]");

}
