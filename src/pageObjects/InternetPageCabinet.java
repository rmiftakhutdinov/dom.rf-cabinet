package pageObjects;

import org.openqa.selenium.By;

public class InternetPageCabinet {

    public By internetAndTV = By.xpath("//p[contains(text(),'Интернет')]");

    public By internetAndTVElement = By.xpath("//h1[text()='Интернет и ТВ ']");

    public By inputAmount = By.xpath("//input[@name='sumTyped']");

    public By pay = By.xpath("//button");

    public By payForm = By.xpath("//p[contains(text(),'Оплата банковской картой')]");

}
