package pageObjects;

import org.openqa.selenium.By;

public class MainMenuPageCabinet {

    public By menu = By.xpath("//div[@class='lk-header_nav']");

    public By closeMenu = By.xpath("//img[@style='cursor: pointer']");

    public By logo = By.xpath("//a[@ui-sref='apartmentsCards']");

    public By apartment = By.xpath("//a[contains(text(),'Апартамент')]");

    public By parking = By.xpath("//a[contains(text(),'Машиноместо')]");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By profile = By.xpath("//div[@class='lk-header_setting icon-settings']");

    public By profileElement = By.xpath("//h2[@class='lk-profile_subtitle ng-binding']");

    public By helpAndContacts = By.xpath("//a[contains(text(),'Помощь и контакты')]");

    public By helpAndContactsElement = By.xpath("//h1[text()='Помощь и контакты']");

    public By promosFromPartners = By.xpath("//a[contains(text(),'Акции от партнеров')]");

    public By promosFromPartnersElement = By.xpath("//h1[text()='Акции от партнеров']");

    public By myOrders = By.xpath("//a[contains(text(),'Мои заказы')]");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By rentParking = By.xpath("//a[contains(text(),'Аренда машиноместа')]");

    public By rentParkingElement = By.xpath("//h1[text()='Аренда машиноместа']");

    public By orderCleaning = By.xpath("//a[contains(text(),'Заказать уборку')]");

    public By orderCleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By orderRepair = By.xpath("//a[contains(text(),'Заказать ремонт')]");

    public By orderRepairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By orderPassesAndKeys = By.xpath("//a[contains(text(),'Пропуска и ключи')]");

    public By orderPassesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By orderOtherServices = By.xpath("//a[contains(text(),'Прочие услуги')]");

    public By orderOtherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

    public By internetAndTV = By.xpath("//a[contains(text(),'Интернет и ТВ')]");

    public By internetAndTVElement = By.xpath("//h1[text()='Интернет и ТВ ']");

    public By invoices = By.xpath("//a[contains(text(),'Счета к оплате')]");

    public By invoicesElement = By.xpath("//h1[text()='Счета к оплате']");

    public By paymentHistory = By.xpath("//a[contains(text(),'История платежей')]");

    public By paymentHistoryElement = By.xpath("//h1[text()='История платежей']");

    public By consumptionHistory = By.xpath("//a[contains(text(),'История потребления')]");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By residencePassport = By.xpath("//a[contains(text(),'Паспорт жилья')]");

    public By residencePassportElement = By.xpath("//h4[text()='Паспорт тестируемого объекта']");

    public By conditionsAndContract = By.xpath("//a[contains(text(),'Условия и договор')]");

    public By conditionsAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By newsAndNotices = By.xpath("//a[contains(text(),'Новости и уведомления')]");

    public By newsAndNoticesElement = By.xpath("//news-list/div/h1");

    public By orderFoodFromRestaurant = By.xpath("//a[contains(text(),'Заказ еды из ресторана')]");

    public By orderFoodFromRestaurantElement = By.xpath("//h2[@class='food-delivery-modal_title']");

    public By closeFoodModal = By.xpath("//div[@class='lk-modal-close']");

}