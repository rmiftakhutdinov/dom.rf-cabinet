package pageObjects;

import org.openqa.selenium.By;

public class MyOrdersPageCabinet {

    public By activeOrderToCancel = By.xpath("//div[@class='orders-wrap']/div[@ng-if='$ctrl.activeOrders.length']/div[1]//p[@class='orders-content_item-title']/a");

    public By canceledOrder = By.xpath("//div[@class='orders-wrap']/div[@ng-if='$ctrl.complitedOrders.length']/div[1]//p[@class='orders-content_item-title']/a");

    public By cancelOrder = By.xpath("//a[text()='Отменить заказ']");

    public By cancelOrderConfirm = By.xpath("//button[text()='Отменить заказ']");

    public By close = By.xpath("//img[@alt='Закрыть']");

    public By submit = By.xpath("//button");

    public By myOrders = By.xpath("//p[text()='Мои заказы']");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By orderCanceledElement = By.xpath("//p[text()='Заказ отменен.']");

    public By orderedOneTimeGuest = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Разовый пропуск для гостей']");

    public By voteFromCardOrder = By.xpath("//button[text()='Оценить']");

    public By voteFromListOrders = By.xpath("//div[@ng-if='$ctrl.complitedOrders.length']/div[1]//button[text()='Оценить']");

    public By votePopUpElement = By.xpath("//h3[text()='Оцените качество работ по 5-бальной системе']");

    public By choiseStar = By.xpath("//div[@class='lk-modal-wrap']//div[@class='star-container']/div[3]");

    public By textareaComment = By.xpath("//textarea");

    public By send = By.xpath("//div[text()='Отправить']");

    public By myOrdersBreadcrumbs = By.xpath("//div[@class='orders-wrap']/a[text()='Мои заказы']");

    public By closeModal = By.xpath("//div[@class='lk-modal-close']");

    public By moveOrder = By.xpath("//button[text()='Перенести заказ']");

    public By inputDateInMyOrders = By.xpath("//service-select[@class='services-select_date ng-isolate-scope']/oi-select/form");

    public By choiseDateInMyOrders = By.xpath("//div[@class='services-select_content']/service-select[1]//div[@class='select-dropdown']/ul/li[3]");

    public By inputTimeInMyOrders = By.xpath("//div[@class='services-select_content']/service-select[2]/oi-select/form");

    public By choiseTimeInMyOrders = By.xpath("//div[@class='services-select_content']/service-select[2]//div[@class='select-dropdown']/ul/li[3]");

    public By inputDate = By.xpath("//service-select[@class='services-select_date ng-scope ng-isolate-scope']/oi-select");

    public By choiseDate = By.xpath("//service-select[@class='services-select_date ng-scope ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By movingAndLoadingElement = By.xpath("//h1[text()='Диагностика']");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By orderPassesAndKeys = By.xpath("//p[contains(text(),'Пропуска')]");

    public By orderPassesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By oneTimePass = By.xpath("//p[text()='Разовый пропуск для гостей']");

    public By textareaNameGuest1 = By.xpath("//div[@class='services-wishes ng-scope']/input[1]");

    public By orderRepair = By.xpath("//div[contains(@ui-sref,'repair')]/p");

    public By orderRepairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By sanitaryWorks = By.xpath("//p[text()='Сантехнические работы']");

    public By sanitaryWorksElement = By.xpath("//h1[text()='Сантехнические работы']");

    public By diagnostic = By.xpath("//p[text()='Диагностика']");

    public By diagnosticElement = By.xpath("//h1[text()='Диагностика']");

    public By repairServicesCart = By.xpath("//p[text()='+ Добавить еще одну услугу']");

    public By choiseDateAndTimeElement = By.xpath("//label[@class='ng-binding'][text()='Выберите удобный день и время']");

    public By orderedDiagnostic = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Диагностика']");

}
