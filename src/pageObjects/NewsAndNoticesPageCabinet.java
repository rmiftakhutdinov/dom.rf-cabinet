package pageObjects;

import org.openqa.selenium.By;

public class NewsAndNoticesPageCabinet {

    public By newsAndNotices = By.xpath("//p[contains(text(),'Новости')]");

    public By newsAndNoticesElement = By.xpath("//news-list/div/h1");

    public By noticeList = By.xpath("//news-list/div/div/a[1]/span[2]");

    public By noticeCard = By.xpath("//news-card/div/p");

    public By breadcrumbs = By.xpath("//news-card/div/a");

}
