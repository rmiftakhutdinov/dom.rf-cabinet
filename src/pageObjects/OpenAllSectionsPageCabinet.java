package pageObjects;

import org.openqa.selenium.By;

public class OpenAllSectionsPageCabinet {

    public By profile = By.xpath("//div[@class='lk-header_setting icon-settings']");

    public By profileElement = By.xpath("//h2[@class='lk-profile_subtitle ng-binding']");

    public By helpAndContacts = By.xpath("//p[contains(text(),'Помощь')]");

    public By helpAndContactsElement = By.xpath("//h1[text()='Помощь и контакты']");

    public By promosFromPartners = By.xpath("//p[contains(text(),'Акции')]");

    public By promosFromPartnersElement = By.xpath("//h1[text()='Акции от партнеров']");

    public By myOrders = By.xpath("//p[text()='Мои заказы']");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By rentParking = By.xpath("//p[contains(text(),'Аренда')]");

    public By rentParkingElement = By.xpath("//h1[text()='Аренда машиноместа']");

    public By orderCleaning = By.xpath("//div[contains(@ui-sref,'clean')]/p");

    public By orderCleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By orderRepair = By.xpath("//div[contains(@ui-sref,'repair')]/p");

    public By orderRepairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By orderPassesAndKeys = By.xpath("//p[contains(text(),'Пропуска')]");

    public By orderPassesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By orderOtherServices = By.xpath("//p[contains(text(),'Прочие')]");

    public By orderOtherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

    public By internetAndTV = By.xpath("//p[contains(text(),'Интернет')]");

    public By internetAndTVElement = By.xpath("//h1[text()='Интернет и ТВ ']");

    public By paymentHistory = By.xpath("//div[contains(@ui-sref,'paymentHistory')]/p");

    public By paymentHistoryElement = By.xpath("//h1[text()='История платежей']");

    public By residencePassport = By.xpath("//p[contains(text(),'Паспорт')]");

    public By residencePassportElement = By.xpath("//h4[text()='Паспорт тестируемого объекта']");

    public By conditionsAndContract = By.xpath("//p[contains(text(),'Условия')]");

    public By conditionsAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By newsAndNotices = By.xpath("//p[contains(text(),'Новости')]");

    public By newsAndNoticesElement = By.xpath("//news-list/div/h1");

    public By orderFoodFromRestaurant = By.xpath("//div[contains(@ng-click,'modalFood')]");

    public By orderFoodFromRestaurantElement = By.xpath("//h2[@class='food-delivery-modal_title']");

    public By invoices = By.xpath("//div[@class='btn btn--green btn--l ng-binding'][text()='Оплатить']");

    public By invoicesElement = By.xpath("//h1[text()='Счета к оплате']");

    public By consumptionHistory = By.xpath("//div[contains(@ui-sref,'consHistory')]/p");

    public By consumptionHistoryElement = By.xpath("//h1[text()='История потребления']");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By parkingA70 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'A70')]");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By jivoClose = By.xpath("//div[@id='jivo_close_button']");

    public By jivoClose2 = By.xpath("//jdiv[@class='closeIcon_1U']");

    public By closeFoodModal = By.xpath("//div[@class='lk-modal-close']");

}
