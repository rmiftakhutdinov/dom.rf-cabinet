package pageObjects;

import org.openqa.selenium.By;

public class OrderOtherServicesPageCabinet {

    public By orderOtherServices = By.xpath("//p[contains(text(),'Прочие')]");

    public By orderOtherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

    public By movingAndLoading = By.xpath("//p[text()='Переезд и погрузка']");

    public By movingAndLoadingElement = By.xpath("//h1[text()='Переезд и погрузка']");

    public By smallOperations = By.xpath("//p[text()='Малогабаритные погрузочно-разгрузочные работы']");

    public By smallOperationsElement = By.xpath("//h2[text()='Малогабаритные погрузочно-разгрузочные работы']");

    public By largeOperations = By.xpath("//p[text()='Крупногабаритные погрузочно-разгрузочные работы']");

    public By largeOperationsElement = By.xpath("//h2[text()='Крупногабаритные погрузочно-разгрузочные работы']");

    public By checkboxScotchPlus = By.xpath("//div[@class='services-sub ng-scope']/div[6]//span[@class='plus-sign']");

    public By checkboxScotchMinus = By.xpath("//div[@class='services-sub ng-scope']/div[6]//div[@class='services-multi ng-scope']/span[1]");

    public By textareaAddress = By.xpath("//div[2]//textarea");

    public By textareaComments = By.xpath("//div[4]//textarea");

    public By submit = By.xpath("//button");

    public By inputDate = By.xpath("//service-select[@class='services-select_date ng-scope ng-isolate-scope']/oi-select");

    public By choiseDate = By.xpath("//service-select[@class='services-select_date ng-scope ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By inputTime = By.xpath("//service-select[@class='services-select_time ng-scope ng-isolate-scope']/oi-select");

    public By choiseTime = By.xpath("//service-select[@class='services-select_time ng-scope ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By myOrders = By.xpath("//button[text()='Мои заказы']");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By orderBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Заказ']");

    public By servicesBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Услуги']");

    public By orderedLargeOperations = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Крупногабаритные погрузочно-разгрузочные работы']");

    public By orderedSmallOperations = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Прочие, 8 услуг']");

    public By priceElement = By.xpath("//p[text()='К указанному месту и времени приедет микроавтобус и два грузчика']");

    public By waterDelivery = By.xpath("//p[text()='Доставка воды']");

    public By waterDeliveryElement = By.xpath("//h1[text()='Доставка воды']");

    public By artesianWater = By.xpath("//p[contains(text(),'Вода артезианская')]");

    public By artesianWaterElement = By.xpath("//h2[contains(text(),'Вода артезианская')]");

    public By waterInfo = By.xpath("//p[contains(text(),'При заказе воды, просим вас не выбрасывать пустые ')]");

    public By plus = By.xpath("//span[@class='services-multi_plus']");

    public By minus = By.xpath("//span[@class='services-multi_minus']");

    public By addComments = By.xpath("//textarea[@name='additional_wishes']");

    public By orderedWater = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[contains(text(),'Вода артезианская')]");

}
