package pageObjects;

import org.openqa.selenium.By;

public class OrderPassesAndKeysPageCabinet {

    public By orderPassesAndKeys = By.xpath("//p[contains(text(),'Пропуска')]");

    public By orderPassesAndKeysElement = By.xpath("//h1[text()='Заказ пропусков и ключей']");

    public By oneTimePass = By.xpath("//p[text()='Разовый пропуск для гостей']");

    public By payPass = By.xpath("//p[text()='Восстановление постоянного пропуска']");

    public By payPassElement = By.xpath("//h2[text()='Восстановление постоянного пропуска']");

    public By textareaNameGuest1 = By.xpath("//div[@class='services-wishes ng-scope']/input[1]");

    public By textareaNameGuest2 = By.xpath("//div[@class='services-wishes ng-scope']/input[2]");

    public By textareaNameGuest3 = By.xpath("//div[@class='services-wishes ng-scope']/input[3]");

    public By submit = By.xpath("//button");

    public By addMoreGuest = By.xpath("//p[text()='+ Добавить еще одного гостя']");

    public By inputDate = By.xpath("//li[@class='btn btn-default btn-xs select-search-list-item select-search-list-item_selection ng-binding ng-scope']");

    public By choiseDate = By.xpath("//oi-select[@class='ng-pristine ng-untouched ng-valid ng-scope ng-isolate-scope ng-not-empty focused open']/div[@class='select-dropdown']/ul/li[3]");

    public By myOrders = By.xpath("//button[text()='Мои заказы']");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By orderBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Заказ']");

    public By servicesBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Услуги']");

    public By orderedOneTimeGuest = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Разовый пропуск для гостей']");

    public By orderedPayPass = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Восстановление постоянного пропуска']");

}
