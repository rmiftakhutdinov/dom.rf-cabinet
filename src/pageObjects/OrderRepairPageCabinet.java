package pageObjects;

import org.openqa.selenium.By;

public class OrderRepairPageCabinet {

    public By orderRepair = By.xpath("//div[contains(@ui-sref,'repair')]/p");

    public By orderRepairElement = By.xpath("//h1[text()='Заказ ремонта']");

    public By sanitaryWorks = By.xpath("//p[text()='Сантехнические работы']");

    public By sanitaryWorksElement = By.xpath("//h1[text()='Сантехнические работы']");

    public By sanitaryWaterSupply = By.xpath("//p[contains(text(),'Водоснабжение и канализация')]");

    public By sanitaryWaterSupplyElement = By.xpath("//h1[contains(text(),'Водоснабжение и канализация')]");

    public By consultation = By.xpath("//p[contains(text(),'Консультация специалиста по водоснабжению')]");

    public By consultationElement = By.xpath("//h2[text()='Консультация специалиста по водоснабжению']");

    public By changeToiletBowl = By.xpath("//p[text()='Замена унитаза']");

    public By changeToiletBowlElement = By.xpath("//h2[text()='Замена унитаза']");

    public By repairServicesCart = By.xpath("//p[text()='+ Добавить еще одну услугу']");

    public By electricWorks = By.xpath("//p[text()='Электротехнические работы']");

    public By electricWorksElement = By.xpath("//h1[text()='Электротехнические работы']");

    public By carpentryWorks = By.xpath("//p[text()='Плотницкие работы']");

    public By carpentryWorksElement = By.xpath("//h1[text()='Плотницкие работы']");

    public By repairSwitch = By.xpath("//p[text()='Ремонт розетки/выключателя']");

    public By repairSwitchElement = By.xpath("//h2[text()='Ремонт розетки/выключателя']");

    public By fittingPeephole = By.xpath("//p[contains(text(),'Врезка дверного глазка')]");

    public By fittingPeepholeElement = By.xpath("//h2[text()='Врезка дверного глазка']");

    public By deleteRepairService = By.xpath("//form[@class='ng-pristine ng-valid']/div[1]/p[@class='services-list_item-trash']");

    public By plus = By.xpath("//span[@class='services-multi_plus']");

    public By minus = By.xpath("//span[@class='services-multi_minus']");

    public By textareaComments = By.xpath("//textarea[@name='wishes']");

    public By submit = By.xpath("//button");

    public By choiseDateAndTimeElement = By.xpath("//label[@class='ng-binding'][text()='Выберите удобный день и время']");

    public By inputDate = By.xpath("//service-select[@class='services-select_date ng-isolate-scope']/oi-select");

    public By choiseDate = By.xpath("//service-select[@class='services-select_date ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By inputTime = By.xpath("//service-select[@class='services-select_time ng-isolate-scope']/oi-select");

    public By choiseTime = By.xpath("//service-select[@class='services-select_time ng-isolate-scope']//div[@class='select-dropdown']/ul/li[3]");

    public By myOrders = By.xpath("//button[text()='Мои заказы']");

    public By serviceOrderedElement = By.xpath("//p[contains(text(),'Заказ оформлен.')]");

    public By orderBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Заказ']");

    public By servicesBreadcrumbs = By.xpath("//span[@class='breadcrumbs-item ng-binding link'][text()='Услуги']");

    public By orderedRepair = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[contains(text(),'Ремонт')]");

    public By orderedConsultation = By.xpath("//div[@class='orders-wrap']//div[@class='orders-content ng-scope']/div[1]//p[@class='orders-content_item-title']/a[text()='Консультация специалиста по водоснабжению']");

}
