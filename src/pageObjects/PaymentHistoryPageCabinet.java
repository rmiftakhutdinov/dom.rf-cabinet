package pageObjects;

import org.openqa.selenium.By;

public class PaymentHistoryPageCabinet {

    public By paymentHistory = By.xpath("//div[contains(@ui-sref,'paymentHistory')]/p");

    public By paymentHistoryElement = By.xpath("//h1[text()='История платежей']");

    public By rent = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Аренда']");

    public By communal = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Коммунальные']");

    public By communalDetail = By.xpath("//div[1]/p/span[contains(text(),'Коммунальные')]");

    public By communalDetailElement = By.xpath("//p[1][contains(text(),'Электричество')]");

    public By services = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Услуги']");

    public By others = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Прочие']");

    public By servicesDetail = By.xpath("//div[@class='payments_table-container ng-scope']/div[1]//div[@class='payments_table-row']/div[1]/p[1]");

    public By servicesDetailElement = By.xpath("//p[text()='Малогабаритные погрузочно-разгрузочные работы']");

    public By allPayments = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][text()='Все']");

    public By apart222 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'222')]");

    public By noPayments = By.xpath("//p[contains(text(),'Нет платежей')]");

}
