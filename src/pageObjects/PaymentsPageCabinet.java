package pageObjects;

import framework.Helper;
import org.openqa.selenium.By;

public class PaymentsPageCabinet extends Helper {

    //public String pageURLAdmin = "https://dev.stage.domrf.orbita.center/admin";
    public String pageURLAdmin = "https://dev.domrf.cometrica.ru/admin";

    public By toInvoicesPage = By.xpath("//div[text()='Оплатить']");

    public By invoicesElement = By.xpath("//h1[text()='Счета к оплате']");

    public By deleteCard = By.xpath("//a[text()='Удалить карту']");

    public By deleteToastAccept = By.xpath("//a[text()='Да']");

    public By breadcrumbsInvoices = By.xpath("//a[@class='link payments_parent-link ng-binding'][text()='Счета к оплате']");

    public By activeRentTerminal = By.xpath("//a[contains(@class,'active')][contains(text(),'Аренда')]");

    public By activeServicesTerminal = By.xpath("//a[contains(@class,'active')][contains(text(),'Услуги')]");

    public By servicesInvoices = By.xpath("//a[@class='payments_filter-item ng-binding ng-scope'][contains(text(),'Услуги')]");

    public By oneInvoice = By.xpath("//div[1]/div/div[@class='payments_table-col']/p/a");

    public By payInvoice = By.xpath("//button[contains(text(),'Оплатить')]");

    public By payAllInvoices = By.xpath("//button[text()='Оплатить все →']");

    public By paidPageElement = By.xpath("//h1[text()='Оплачено']");

    public By errorPayElement = By.xpath("//h1[text()='Ошибка оплаты']");

    public By saveDataCard = By.xpath("//div[@class='control control--checkbox']/label");

    public By choiseCard = By.xpath("//div[@class='fx-block']");

    public By newCard = By.xpath("//oi-select/div/ul/li/p[text()='Новая карта']");

    public By paymentsHistoryElement = By.xpath("//h1[text()='История платежей']");

    public By noInvoicesElement = By.xpath("//p[text()='Нет счетов. Счета за аренду и коммунальные услуги формируются раз в месяц.']");

    public By menu = By.xpath("//div[@class='lk-header_nav']");

    public By invoicesMenu = By.xpath("//a[contains(text(),'Счета к оплате')]");

        // Метод выбора новой карты
        protected void selectNewCard(){

            PaymentsPageCabinet page = new PaymentsPageCabinet();

            click(page.choiseCard);
            click(page.newCard);
            click(page.payInvoice);

        }

        // Метод удаления карты
        protected void deleteCard(){

            PaymentsPageCabinet page = new PaymentsPageCabinet();

            click(page.deleteCard);
            click(page.deleteToastAccept);
            waitForElementIsNotPresent(page.deleteToastAccept);

        }

    // Терминал по аренде
    public By roskapElement = By.xpath("//h1[text()='Оплата']");
    public By numberCard = By.xpath("//input[@id='number']");
    public By monthCard = By.xpath("//div[@class='choices__inner choices__inner_month']");
    public By yearCard = By.xpath("//div[@class='choices__inner choices__inner_year']");
    public By codCard = By.xpath("//input[@name='CVC2']");
    public By ownerCard = By.xpath("//input[@name='NAME']");
    public By chooseMonth = By.xpath("//div[@id='choices-EMonth-item-choice-9']");
    public By chooseYear = By.xpath("//div[@id='choices-EYear-item-choice-2']");
    public By pay = By.xpath("//button[@name='SUBMIT']");
    public By toPaymentsHistoryRoskap = By.xpath("//a[contains(text(),'История платежей')]");

    // Терминал по услугам
    public By paytureElement = By.xpath("//div[text()='Оплата']");
    public By inputCardNumber = By.xpath("//input[@name='CardNumber']");
    public By choiseDateMonthCard = By.xpath("//li[@id='ui-id-12']");
    public By choiseDateYearCard = By.xpath("//li[@id='ui-id-16']");
    public By inputDateMonthCard = By.xpath("//div[@class='input dateMonth']/span[@id='EMonth-button']");
    public By inputDateYearCard = By.xpath("//div[@class='input dateYear']/span[@id='EYear-button']");
    public By cvcCodCard = By.xpath("//input[@id='cvv']");
    public By ownerNameCard = By.xpath("//input[@name='CardHolder']");
    public By backToPayForm = By.xpath("//a[@id='backPayment']");
    public By notPaidElement = By.xpath("//h1[text()='Не оплачено']");
    public By toInvoices = By.xpath("//div[@class='payments_btn-group payments_btn-group--center']/a[text()='Счета к оплате']");
    public By toPaymentsHistoryPayture = By.xpath("//p[@class='text payments_status-text']/a[contains(text(),'История платежей')]");

    // Админка - новый счет
    public By addInvoiceElement = By.xpath("//h1[text()='Новый счет']");
    public By dropDownRenter = By.xpath("//ol[@id='dynamic-options']/button");
    public By inputChoiseRenter = By.xpath("//input[@class='form-control']");
    public By objectNumberSelect = By.xpath("//form[@name='invoiceForm']/div[2]//select");
    public By invoicePurpose = By.xpath("//input[@name='purpose']");
    public By invoiceTypeSelect = By.xpath("//form[@name='invoiceForm']/div[4]//select");
    public By sumInvoice = By.xpath("//input[@name='sum']");
    public By createInvoice = By.xpath("//button[text()='Сформировать счет']");
    public By toastNewInvoice = By.xpath("//div[@class='toast-message'][text()='Счет успешно сформирован']");

}
