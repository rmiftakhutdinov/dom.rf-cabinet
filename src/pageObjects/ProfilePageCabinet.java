package pageObjects;

import org.openqa.selenium.By;

public class ProfilePageCabinet {

    public By profile = By.xpath("//div[@class='lk-header_setting icon-settings']");

    public By profileElement = By.xpath("//h2[@class='lk-profile_subtitle ng-binding']");

    public By email = By.xpath("//input[@name='email']");

    public By phone = By.xpath("//input[@name='phone']");

    public By oldPass = By.xpath("//input[@name='current_password']");

    public By newPass = By.xpath("//input[@name='new']");

    public By save = By.xpath("//button");

    public By eyeOld = By.xpath("//form[@name='$ctrl.profileForm']/div[4]//div[@class='s-ico s-ico-eye input-eye']");

    public By eyeNew = By.xpath("//form[@name='$ctrl.profileForm']/div[5]//div[@class='s-ico s-ico-eye input-eye']");

    public By logout = By.xpath("//div[@class='lk-header_logout icon-logout']");

    public By authFormElement = By.xpath("//h1[text()='Вход в личный кабинет жильца']");

    public By changeToEnglish = By.xpath("//label[@for='en']");

    public By changeToRussian = By.xpath("//label[@for='ru']");

    public By rusElement = By.xpath("//h2[@class='lk-profile_subtitle ng-binding'][text()='Изменение пароля']");

    public By engElement = By.xpath("//h2[@class='lk-profile_subtitle ng-binding'][text()='Change password']");

}
