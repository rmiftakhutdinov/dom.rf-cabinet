package pageObjects;

import org.openqa.selenium.By;

public class RentParkingPageCabinet {

    public By rentParking = By.xpath("//p[contains(text(),'Аренда')]");

    public By rentParkingElement = By.xpath("//h1[text()='Аренда машиноместа']");

    public By breadcrumbsServices = By.xpath("//span[text()='Услуги']");

    public By otherServicesElement = By.xpath("//h1[text()='Заказ прочих услуг']");

}
