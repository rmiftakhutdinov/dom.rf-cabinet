package pageObjects;

import org.openqa.selenium.By;

public class SwitcherPageCabinet {

    public By admin = By.xpath("//p[text()='Администратор']");

    public By changeUser = By.xpath("//a[text()='Переключить пользователя']");

    public By userListElement = By.xpath("//h2[text()='Переключить пользователя']");

    public By search = By.xpath("//input[@name='search']");

    public By logInAsUser = By.xpath("//div[text()='Войти под пользователем']");

    public By userName306 = By.xpath("//p[text()='Наблюдатель А.']");

    public By backToMyAccount = By.xpath("//a[text()='Вернуться в свой аккаунт']");

    public By myUserName = By.xpath("//p[text()='Мифтахутдинов Р.']");

    public By payments = By.xpath("//div[text()='Оплатить']");

    public By payAll = By.xpath("//button");

    public By errorToast = By.xpath("//div[@class='toast-message']/small[text()='Ошибка 403 - Превышение полномочий']");

    public By closeErrorToast = By.xpath("//button[@class='toast-close-button ng-scope']");

    public By foundUserElement306 = By.xpath("//div[@class='fio ng-binding'][text()='Наблюдатель Арендатор']");

    public By apart250 = By.xpath("//div[contains(@class,'swiper-slide-active')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'250')]");

    public By apart220 = By.xpath("//div[contains(@class,'swiper-slide ng-scope')]//h1[@class='lk-cover_title ng-binding'][contains(text(),'220')]");

    public By nextObject = By.xpath("//img[@src='./images/appartment-arrow-right@2x.png']");

    public By orderCleaning = By.xpath("//div[contains(@ui-sref,'clean')]/p");

    public By orderCleaningElement = By.xpath("//h1[text()='Заказ уборки']");

    public By maintainingCleaning = By.xpath("//p[text()='Поддерживающая уборка']");

    public By maintainingCleaningElement = By.xpath("//h2[text()='Поддерживающая уборка']");

    public By submit = By.xpath("//button");

    public By choiseDateAndTimeElement = By.xpath("//label[@class='ng-binding'][text()='Выберите удобный день и время']");

    public By choiseAnotherDateAndTimeElement = By.xpath("//label[@class='ng-binding'][text()='Выберите другой день и время']");

    public By myOrders = By.xpath("//p[text()='Мои заказы']");

    public By myOrdersElement = By.xpath("//h1[text()='Мои заказы']");

    public By generalCleaning = By.xpath("//a[text()='Генеральная уборка']");

    public By changeOrder = By.xpath("//button[text()='Перенести заказ']");

    public By cancelOrder = By.xpath("//a[text()='Отменить заказ']");

    public By cancelOrderConfirm = By.xpath("//button[text()='Отменить заказ']");

    public By voteOrder = By.xpath("//div[@ng-if='$ctrl.complitedOrders.length']/div[1]//button[text()='Оценить']");

    public By votePopUpElement = By.xpath("//h3[text()='Оцените качество работ по 5-бальной системе']");

    public By choiseStar = By.xpath("//div[@class='lk-modal-wrap']//div[@class='star-container']/div[3]");

    public By send = By.xpath("//div[text()='Отправить']");

    public By sendChangeDate = By.xpath("//a[text()='Отправить']");

    public By conditionsAndContract = By.xpath("//p[contains(text(),'Условия')]");

    public By conditionsAndContractElement = By.xpath("//h1[text()='Условия и договор']");

    public By informDateDeparture = By.xpath("//button[text()='Cообщить дату выезда']");

    public By dateDeparture = By.xpath("//h1[contains(text(),'Дата выезда')]");

}
