package tests.authorizationFormTestCabinet;

import data.CommonData;
import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import java.util.concurrent.TimeUnit;

public class AuthorizationFormTestCabinet extends Helper {

    @BeforeMethod
    public void setUp(){

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Кликнуть на логотип")
    public void clickLogo() {

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();

        try {
            findElement(page.authPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница авторизации не загрузилась");
        }

        click(page.logo);
        try {
            findElement(page.homePageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }

    }

    @Test(description = "Открыть документ 'Условия использования'", priority = 1)
    public void openTermsOfUse() throws InterruptedException {

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        String handleBefore = driver.getWindowHandle();

        try {
            findElement(page.authPageElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница авторизации не загрузилась");
        }

        click(page.termsOfUse);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String id = "https://dev-bk.stage.domrf.orbita.center/documents/terms_of_service.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));
        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Test(description = "Авторизация по номеру телефона", priority = 2)
    public void authorizationByPhone() {

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        findElement(page.userName).sendKeys(user.userPhoneCab);
        findElement(page.userPassword).sendKeys(user.userPass);
        Assert.assertTrue(driver.findElement(page.userPassword).getAttribute("type").equals("password"));
        click(page.eye);
        Assert.assertTrue(driver.findElement(page.userPassword).getAttribute("type").equals("text"));

        click(page.submit);

        try {
            findElement(page.cabinetElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Личный кабинет не загрузился");
        }

    }

    @Test(description = "Отправка ссылки на эл.почту для восстановления пароля", priority = 3)
    public void changePasswordByEmail() {

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();

        click(page.changePassword);
        try {
            findElement(page.backToAuthPage);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Форма ввода почты и телефона не загрузилась");
        }

        findElement(page.input).sendKeys("rm@cometrica.ru");

        click(page.nextPage);
        try {
            findElement(page.sendLinkAgain);
        } catch (NoSuchElementException ex) {
            afterExeptionMessage("Форма повторной отправки ссылки на эл.почту не появилась");
        }

        click(page.sendLinkAgain);
        Assert.assertTrue(findElement(page.spinner).isDisplayed());
        Assert.assertTrue(findElement(page.sendLinkAgain).isDisplayed());

    }

    @Test(description = "Отправка кода на номер телефона для восстановления пароля", priority = 4)
    public void changePasswordByPhoneNumber() {

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        CommonData user = new CommonData();

        click(page.changePassword);
        try {
            findElement(page.backToAuthPage);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Форма ввода почты и телефона не загрузилась");
        }

        findElement(page.input).sendKeys(user.userPhoneCab);

        click(page.nextPage);
        try {
            findElement(page.sendCodPage);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Форма ввода кода не загрузилась");
        }

        waitBy(page.changePhoneNumber, 70);

        click(page.changePhoneNumber);
        try {
            findElement(page.backToAuthPage);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Форма ввода почты и телефона не загрузилась");
        }

        click(page.nextPage);
        try {
            findElement(page.textRequestCodAgain);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подсказка о повторном запросе пароля не появилась");
        }

        waitBy(page.sendCodAgain, 70);

        click(page.sendCodAgain);
        try {
            findElement(page.textRequestCodAgain);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подсказка о повторном запросе пароля не появилась");
        }

    }

}
