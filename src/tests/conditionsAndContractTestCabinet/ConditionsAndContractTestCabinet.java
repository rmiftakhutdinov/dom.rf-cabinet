package tests.conditionsAndContractTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.ConditionsAndDocumentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class ConditionsAndContractTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Открыть 'Правила проживания' в разделе 'Условия и договор'")
    public void openRulesOfResidence() throws InterruptedException {

        ConditionsAndDocumentsPageCabinet page = new ConditionsAndDocumentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        String handleBefore = driver.getWindowHandle();

        scrollDown();

        click(page.conditionsAndContract);
        try {
            findElement(page.conditionsAndContractElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Условия и договор' не загрузилась");
        }

        click(page.rulesOfResidence);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String id = "https://dev-bk.stage.domrf.orbita.center/documents/living_rules.pdf";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));
        driver.close();
        driver.switchTo().window(handleBefore);

    }

    @Ignore // Данный функционал убран
    @Test(description = "Назначение и отмена даты выезда - апарт №250", priority = 1)
    public void setAndCanceledDateDeparture() throws InterruptedException {

        ConditionsAndDocumentsPageCabinet page = new ConditionsAndDocumentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(page.apart250)){

                break;

            }

            click(page.nextObject);

        }

        scrollDown();
        click(page.conditionsAndContract);
        findElement(page.informDateDeparture).click();

        findElement(page.dateDeparture);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.submit);
        findElement(page.close).click();
        sleep(500);
        Assert.assertFalse(driver.findElement(page.close).isDisplayed());

        click(page.submit);
        findElement(page.confirm).click();
        findElement(page.cancelDeparture).click();

        findElement(page.close).click();
        sleep(500);
        Assert.assertFalse(driver.findElement(page.close).isDisplayed());

        click(page.cancelDeparture);
        findElement(page.confirm).click();
        findElement(page.informDateDeparture);

    }

    @Ignore // Данный функционал убран
    @Test(description = "Расторжение и отмена расторжения договора аренды - машиноместо №70", priority = 2)
    public void terminatedAndCanceledContractOfRent() throws InterruptedException {

        ConditionsAndDocumentsPageCabinet page = new ConditionsAndDocumentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(page.parkingA70)){

                break;

            }

            click(page.nextObject);

        }

        click(page.conditionsAndContract);
        findElement(page.terminatedContractOfRent).click();

        findElement(page.dateTerminatedContract);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.submit);
        findElement(page.close).click();
        sleep(500);
        Assert.assertFalse(findElement(page.close).isDisplayed());

        click(page.submit);
        findElement(page.confirm).click();
        findElement(page.cancelTerminated).click();

        findElement(page.close).click();
        sleep(500);
        Assert.assertFalse(findElement(page.close).isDisplayed());

        click(page.cancelTerminated);
        findElement(page.confirm).click();
        findElement(page.terminatedContractOfRent);

    }

    @Test(description = "Открыть галерею договора аренды", priority = 3)
    public void openGalleryInContractOfRent() throws InterruptedException {

        ConditionsAndDocumentsPageCabinet page = new ConditionsAndDocumentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(page.apart250)){

                break;

            }

            click(page.nextObject);

        }

        click(page.conditionsAndContract);
        try {
            waitBy(page.conditionsAndContractElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

        click(page.contractOfRent);
        try {
            waitBy(page.contractOfRentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страница галереи не найден");
        }

        click(page.img);
        try {
            waitBy(page.openJPG);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент открытой фото в режиме 'галерея' не найден");
        }

        String img1 = new String(findElement(page.openJPG).getAttribute("src"));
        click(page.nextImg);
        String img2 = new String(findElement(page.openPNG).getAttribute("src"));
        Assert.assertFalse(img1.equals(img2));

        click(page.prevImg);
        Assert.assertFalse(img2.equals(img1));

        click(page.closeImg);
        waitForElementIsNotPresent(page.closeImg);

        String handleBefore = driver.getWindowHandle();

        click(page.pdf);
        sleep(3000);
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
        String id = "https://dev-bk.stage.domrf.orbita.center/uploads/media/file/contract/";
        Assert.assertTrue(driver.getCurrentUrl().contains(id));
        Assert.assertTrue(driver.getCurrentUrl().contains(".pdf"));
        driver.close();
        driver.switchTo().window(handleBefore);

        click(page.breadcrumbs);
        try {
            waitBy(page.conditionsAndContractElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент раздела 'Условия и договор' не найден");
        }

    }

}