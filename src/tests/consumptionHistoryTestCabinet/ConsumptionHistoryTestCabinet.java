package tests.consumptionHistoryTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.ConsumptionHistoryPageCabinet;
import java.util.concurrent.TimeUnit;

public class ConsumptionHistoryTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Просмотр потребления в разделе 'История потребления'")
    public void viewConsumptionHistory() throws InterruptedException {

        ConsumptionHistoryPageCabinet page = new ConsumptionHistoryPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart222)) {

            click(auth2.nextObject);

        }

        click(page.consumptionHistory);
        try {
            findElement(page.consumptionHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История потребления' не загрузился");
        }

        //consumptionHistoryDevStand();

        consumptionHistoryTestStand();

    }

}
