package tests.faqTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.HelpAndContactsPageCabinet;
import java.util.concurrent.TimeUnit;

public class FAQTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Свернуть и развернуть статьи в разделе 'Помощь и контакты'")
    public void hideAndShowFAQ() throws InterruptedException {

        HelpAndContactsPageCabinet page = new HelpAndContactsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        click(page.FAQ);
        try {
            findElement(page.FAQElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница 'Помощь и контакты' не загрузилась");
        }

        scrollToElement(page.parking);
        sleep(500);
        click(page.parking);
        sleep(500);
        Assert.assertTrue(findElement(page.parkingElement).getText().contains("Для арендных апартаментов"));
        click(page.parking);
        sleep(500);
        try {
            Assert.assertFalse(findElement(page.parkingElement).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Статья в разделе 'Общие' не свернулась");
        }

        scrollToElement(page.apartments);
        sleep(500);
        click(page.apartments);
        sleep(500);

        scrollToElement(page.internet);
        sleep(500);
        click(page.internet);
        sleep(500);
        Assert.assertTrue(findElement(page.internetElement).getText().contains("Во всех апартаментах проведен"));
        click(page.internet);
        sleep(500);
        try {
            Assert.assertFalse(findElement(page.internetElement).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Статья в разделе 'Апартаменты' не свернулась");
        }

        click(page.apartments);
        sleep(500);
        try {
            Assert.assertFalse(findElement(page.internet).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Раздел 'Апартаменты' не свернулся");
        }

    }

}
