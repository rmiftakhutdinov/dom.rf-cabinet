package tests.internetTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.InternetPageCabinet;
import java.util.concurrent.TimeUnit;

public class InternetTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Ignore // Не используется
    @Test(description = "Оплата услуг 'Интернет и ТВ'")
    public void paymentInternetAndTVServices() throws InterruptedException {

        InternetPageCabinet page = new InternetPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.internetAndTV);
        try {
            findElement(page.internetAndTVElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Интернет и ТВ' не загрузился");
        }

        findElement(page.inputAmount).clear();
        findElement(page.inputAmount).sendKeys("1");

        click(page.pay);
        try {
            findElement(page.payForm);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход в форму оплаты не осуществился");
        }

    }

}
