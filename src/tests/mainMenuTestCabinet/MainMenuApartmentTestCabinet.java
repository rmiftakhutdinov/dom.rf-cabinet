package tests.mainMenuTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.MainMenuPageCabinet;
import java.util.concurrent.TimeUnit;

public class MainMenuApartmentTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Главное меню апартамента")
    public void mainMenuApartment() throws InterruptedException {

        MainMenuPageCabinet page = new MainMenuPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.profile);
        try {
            waitBy(page.profileElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Профиль жильца не загрузился");
        }

        click(page.menu);
        sleep(1000);
        findElement(page.closeMenu);
        click(page.profile);
        sleep(1000);
        Assert.assertFalse(findElement(page.logo).isDisplayed());

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.helpAndContacts);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.helpAndContactsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Помощь и контакты' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.promosFromPartners);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.promosFromPartnersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Акции от партнеров' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.myOrders);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.myOrdersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Мои заказы' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.rentParking);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.rentParkingElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Аренда машиноместа' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.orderCleaning);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.orderRepair);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.orderPassesAndKeys);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.orderOtherServices);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.orderOtherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);

        // раздел 'Интернет и ТВ' отсутствует в апартаменте
        /*click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.internetAndTV);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.internetAndTVElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Интернет и ТВ' не загрузился");
        }*/

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.invoices);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.invoicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Счета к оплате' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.paymentHistory);
        try {
            waitForElementIsNotPresent(page.paymentHistory, 5);
            waitBy(page.paymentHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.consumptionHistory);
        try {
            waitForElementIsNotPresent(page.consumptionHistory, 5);
            waitBy(page.consumptionHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История потребления' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.residencePassport);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.residencePassportElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Паспорт жилья' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.conditionsAndContract);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.conditionsAndContractElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Условия и договор' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.newsAndNotices);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.newsAndNoticesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Новости и уведомления' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.orderFoodFromRestaurant);
        try {
            waitBy(page.orderFoodFromRestaurantElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Модальное окно заказа еды из ресторана не открылось");
        }

        click(page.closeFoodModal);
        sleep(1000);

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.apartment);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.apart250);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Апартамент не найден");
        }

        click(page.nextObject);

        for (;;){

            if (isElementPresent(auth.parkingA70)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.profile);
        findElement(page.profileElement);
        sleep(1000);
        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.parking);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.parkingA70);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Машиноместо не найдено");
        }

    }

}
