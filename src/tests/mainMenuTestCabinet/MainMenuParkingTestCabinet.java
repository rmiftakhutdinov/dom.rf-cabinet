package tests.mainMenuTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.MainMenuPageCabinet;
import java.util.concurrent.TimeUnit;

public class MainMenuParkingTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Главное меню машиноместа")
    public void mainMenuParking() throws InterruptedException {

        MainMenuPageCabinet page = new MainMenuPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.parkingA70)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.profile);
        try {
            findElement(page.profileElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Профиль жильца не загрузился");
        }

        click(page.menu);
        sleep(1000);
        findElement(page.closeMenu);
        click(page.profile);
        sleep(1000);
        Assert.assertFalse(findElement(page.logo).isDisplayed());

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.helpAndContacts);
        try {
            findElement(page.helpAndContactsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Помощь и контакты' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.invoices);
        try {
            waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 30);
            waitBy(page.invoicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Счета к оплате' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.paymentHistory);
        try {
            waitForElementIsNotPresent(page.paymentHistory, 5);
            findElement(page.paymentHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.conditionsAndContract);
        try {
            findElement(page.conditionsAndContractElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Условия и договор' не загрузился");
        }

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.parking);
        try {
            findElement(page.parkingA70);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Машиноместо не найдено");
        }

        click(page.nextObject);

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.profile);
        findElement(page.profileElement);
        sleep(1000);

        click(page.menu);
        sleep(1000);
        Assert.assertTrue(findElement(page.logo).isDisplayed());

        click(page.apartment);
        try {
            findElement(page.apart250);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Апартамент не найден");
        }

    }

}
