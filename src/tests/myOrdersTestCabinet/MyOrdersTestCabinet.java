package tests.myOrdersTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.MyOrdersPageCabinet;
import pageObjects.OrderRepairPageCabinet;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class MyOrdersTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Оценка заказа из карточки заказа")
    public void voteFromCardOrder() throws InterruptedException {

        MyOrdersPageCabinet page = new MyOrdersPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        try {
            findElement(page.textareaNameGuest1);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора даты визита не загрузилась");
        }

        findElement(page.textareaNameGuest1).sendKeys("Иванов");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

        click(page.submit);
        try {
            findElement(page.orderedOneTimeGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedOneTimeGuest);

        new WebDriverWait(driver, 30).until(ExpectedConditions.urlContains("card"));

        click(page.voteFromCardOrder);
        findElement(page.votePopUpElement);

        click(page.closeModal);
        waitForElementIsNotPresent(page.closeModal);

        click(page.voteFromCardOrder);
        findElement(page.votePopUpElement);

        click(page.choiseStar);
        findElement(page.textareaComment).sendKeys("Коммент отобразится в админке по ховеру на звездочке");
        click(page.send);

        List<WebElement> activeStars = driver.findElements(By.cssSelector("div.star-container>div>svg.star-icon.active"));
        Assert.assertTrue(activeStars.size() == 3);

        waitForElementIsNotPresent(By.xpath("//div[@class='orders-valuation']"), 30);

        click(page.myOrdersBreadcrumbs);
        Assert.assertTrue(findElement(page.myOrdersElement).isDisplayed());

    }

    @Test(description = "Оценка заказа из списка заказов", priority = 1)
    public void voteFromListOrders() throws InterruptedException {

        MyOrdersPageCabinet page = new MyOrdersPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        try {
            findElement(page.textareaNameGuest1);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора даты визита не загрузилась");
        }

        findElement(page.textareaNameGuest1).sendKeys("Иванов");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

        click(page.submit);
        try {
            findElement(page.orderedOneTimeGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.voteFromListOrders);
        findElement(page.votePopUpElement);

        click(page.closeModal);
        waitForElementIsNotPresent(page.closeModal);

        click(page.voteFromListOrders);
        findElement(page.votePopUpElement);

        click(page.choiseStar);
        findElement(page.textareaComment).sendKeys("Коммент отобразится в админке по ховеру на звездочке");
        click(page.send);

        List<WebElement> activeStars = driver.findElements(By.xpath("//div[@ng-if='$ctrl.complitedOrders.length']/div[2]//div[@class='star-container']/div/child::*[contains(@class,'star-icon active')]"));
        Assert.assertTrue(activeStars.size() == 3);

    }

    @Test(description = "Отмена заказа", priority = 2)
    public void cancelOrder() throws InterruptedException {

        MyOrdersPageCabinet page = new MyOrdersPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.myOrders);
        findElement(page.myOrdersElement);

        String activeOrder = findElement(page.activeOrderToCancel).getText();
        click(page.activeOrderToCancel);
        findElement(page.cancelOrder);

        click(page.cancelOrder);
        findElement(page.close);
        click(page.close);
        waitForElementIsNotPresent(page.close);

        click(page.cancelOrder);
        findElement(page.cancelOrderConfirm);
        click(page.cancelOrderConfirm);
        findElement(page.orderCanceledElement);

        sleep(1000);

        click(page.submit);
        waitBy(page.myOrdersElement);
        waitBy(page.canceledOrder);
        String canceledOrder = findElement(page.canceledOrder).getText();
        try {
            Assert.assertTrue(activeOrder.equals(canceledOrder));
        } catch (AssertionError ex){
            afterExeptionMessage("Заказ не был отменен");
        }

    }

    @Test(description = "Перенести заказ на другую дату и время", priority = 3)
    public void changeDateAndTimeOrder() throws InterruptedException {

        MyOrdersPageCabinet page = new MyOrdersPageCabinet();
        OrderRepairPageCabinet page2 = new OrderRepairPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderRepair);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.sanitaryWorks);
        try {
            findElement(page.sanitaryWorksElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Сантехнические работы' не загрузился");
        }

        click(page2.sanitaryWaterSupply);
        try {
            findElement(page2.sanitaryWaterSupplyElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Водоснабжение и канализация' не загрузился");
        }

        click(page2.consultation);
        try {
            findElement(page2.consultationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        click(page.submit);
        findElement(page.choiseDateAndTimeElement);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

        click(page2.myOrders);
        try {
            findElement(page2.orderedConsultation);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page2.orderedConsultation);
        try {
            findElement(page.moveOrder);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки 'Перенести заказ' не найден");
        }

        click(page.moveOrder);
        findElement(page2.consultationElement);

        click(page.inputDateInMyOrders);
        sleep(100);
        click(page.choiseDateInMyOrders);

        click(page.inputTimeInMyOrders);
        sleep(100);
        click(page.choiseTimeInMyOrders);

        click(page.moveOrder);
        waitBy(auth.spinner);
        waitForElementIsNotPresent(auth.spinner);
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();

        click(page.submit);
        waitBy(page2.orderedConsultation);

        click(page2.orderedConsultation);
        waitBy(page.moveOrder);

        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try {
            Assert.assertEquals(dateOrder1,dateOrder2);
        } catch (AssertionError ex){
            afterExeptionMessage("Дата и время заказа не были перенесены");
        }

    }

}
