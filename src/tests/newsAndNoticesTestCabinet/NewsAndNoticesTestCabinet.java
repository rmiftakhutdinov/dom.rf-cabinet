package tests.newsAndNoticesTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.NewsAndNoticesPageCabinet;
import java.util.concurrent.TimeUnit;

public class NewsAndNoticesTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Открыть уведомление в разделе 'Новости и уведомления'")
    public void openNotice() throws InterruptedException {

        NewsAndNoticesPageCabinet page = new NewsAndNoticesPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.newsAndNotices);
        try {
            findElement(page.newsAndNoticesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Новости и уведомления' не загрузился");
        }

        String noticeList = findElement(page.noticeList).getText().replace("…", "").trim();

        click(page.noticeList);
        try {
            findElement(page.noticeCard);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на карточку уведомления не осуществился");
        }

        String noticeCard = findElement(page.noticeCard).getText();
        Assert.assertTrue(noticeCard.contains(noticeList));

        click(page.breadcrumbs);
        try {
            findElement(page.newsAndNoticesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Новости и уведомления' не загрузился");
        }

    }

}
