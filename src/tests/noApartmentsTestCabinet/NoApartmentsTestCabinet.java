package tests.noApartmentsTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.NoApartmentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class NoApartmentsTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Нет апартаментов")
    public void noApartments() throws InterruptedException {

        NoApartmentsPageCabinet page = new NoApartmentsPageCabinet();
        AuthorizationPageCabinet auth3 = new AuthorizationPageCabinet();

        waitBy(auth3.submit, 30);
        authorizationThird();

        try {
            findElement(page.noApartmentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент личного кабинета без апартаментов");
        }

    }

}
