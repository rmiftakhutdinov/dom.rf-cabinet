package tests.noDataApartmentsTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import java.util.concurrent.TimeUnit;
import pageObjects.NoDataApartmentsPageCabinet;

public class NoDataApartmentsTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Переключение между апартаментами")
    public void changeObjects() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();


        for (;;){

            if (isElementPresent(page.apart222)){

                try {
                    waitBy(page.apart222);
                } catch (TimeoutException ex){
                    afterExeptionMessage("Апартамент №222 не найден");
                }

                break;

            }

            click(page.nextObject);

        }

        for (;;){

            if (isElementPresent(page.apart221)){

                try {
                    waitBy(page.apart221);
                } catch (TimeoutException ex){
                    afterExeptionMessage("Апартамент №221 не найден");
                }

                break;

            }

            click(page.nextObject);

        }

    }

    @Test(description = "Переход в разделы заказа услуг из раздела 'Мои заказы'", priority = 1)
    public void goToOrderServices() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart221)) {

            click(page.nextObject);

        }

        click(page.myOrders);
        try {
            findElement(page.myOrdersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Мои заказы' не загрузился");
        }

        click(page.orderCleaning);
        try {
            findElement(page.cleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.myOrdersElement, 20);

        click(page.orderRepair);
        try {
            findElement(page.repairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.myOrdersElement, 20);

        click(page.orderPassesAndKeys);
        try {
            findElement(page.passesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.myOrdersElement, 20);

        click(page.orderOtherServices);
        try {
            findElement(page.otherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.myOrdersElement, 20);

    }

    @Test(description = "Переключение вкладок в разделе 'История платежей' в апартаменте", priority = 2)
    public void paymentHistoryInApartment() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart221)) {

            click(page.nextObject);

        }

        click(page.paymentHistory);
        try {
            findElement(page.paymentHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }

        click(page.rent);
        try {
            findElement(page.rentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Аренда' не произошел");
        }

        click(page.communal);
        try {
            findElement(page.communalElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Коммунальные' не произошел");
        }

        click(page.services);
        try {
            findElement(page.servicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Услуги' не произошел");
        }

        click(page.other);
        try {
            findElement(page.otherElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Прочие' не произошел");
        }

        click(page.allPayments);
        try {
            findElement(page.allPaymentsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Все' не произошел");
        }

    }

    @Test(description = "Переключение вкладок в разделе 'История платежей' в машиноместе", priority = 3)
    public void paymentHistoryInParking() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.parkingA70)) {

            click(page.nextObject);

        }

        click(page.paymentHistory);
        try {
            findElement(page.paymentHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }

        click(page.rent);
        try {
            findElement(page.rentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Аренда' не произошел");
        }

        click(page.services);
        try {
            findElement(page.servicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Услуги' не произошел");
        }

        click(page.other);
        try {
            findElement(page.otherElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Прочие' не произошел");
        }

        click(page.allPayments);
        try {
            findElement(page.allPaymentsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на вкладку 'Все' не произошел");
        }

    }

    @Test(description = "Переключение вкладок в разделе 'История потребления'", priority = 4)
    public void consumptionNoHistory() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart221)) {

            click(page.nextObject);

        }

        click(page.consumptionHistory);
        try {
            findElement(page.consumptionHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История потребления' не загрузился");
        }

        String coldWater = findElement(page.coldWater).getText();
        click(page.coldWater);
        String coldWaterElement = findElement(By.xpath("//a[2][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(coldWater.contains(coldWaterElement));
        waitBy(page.noHistoryElement);

        String hotWater = findElement(page.hotWater).getText();
        click(page.hotWater);
        String hotWaterElement = findElement(By.xpath("//a[3][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(hotWater.contains(hotWaterElement));
        waitBy(page.noHistoryElement);

        String electric = findElement(page.electric).getText();
        click(page.electric);
        String electricElement = findElement(By.xpath("//a[1][@class='payments_filter-item ng-binding ng-scope payments_filter-item--active']")).getText();
        Assert.assertTrue(electric.contains(electricElement));
        waitBy(page.noHistoryElement);

    }

    @Test(description = "Нет счетов для оплаты", priority = 5)
    public void noPayments() throws InterruptedException {

        NoDataApartmentsPageCabinet page = new NoDataApartmentsPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart222)) {

            click(page.nextObject);

        }

        click(page.profile);
        try {
            findElement(page.profileElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Профиль жильца не загрузился");
        }

        click(page.menu);
        sleep(500);
        click(page.payments);
        try {
            findElement(page.noPaymentsElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Счета к оплате' не загрузился");
        }

    }

}