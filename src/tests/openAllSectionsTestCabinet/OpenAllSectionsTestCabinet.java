package tests.openAllSectionsTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.TimeoutException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OpenAllSectionsPageCabinet;
import java.util.concurrent.TimeUnit;

public class OpenAllSectionsTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Открыть все разделы апартамента")
    public void openAllSectionsOfApartment() throws InterruptedException {

        OpenAllSectionsPageCabinet page = new OpenAllSectionsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(page.apart250)){

                break;

            }

            click(page.nextObject);

        }

        click(page.profile);
        try {
            waitBy(page.profileElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Профиль жильца не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.helpAndContacts);
        try {
            waitBy(page.helpAndContactsElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Помощь и контакты' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.promosFromPartners);
        try {
            waitBy(page.promosFromPartnersElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Акции от партнеров' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.myOrders);
        try {
            waitBy(page.myOrdersElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Мои заказы' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.rentParking);
        try {
            waitBy(page.rentParkingElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Аренда машиноместа' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.orderCleaning);
        try {
            waitBy(page.orderCleaningElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.orderRepair);
        try {
            waitBy(page.orderRepairElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.orderPassesAndKeys);
        try {
            waitBy(page.orderPassesAndKeysElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        if (driver.findElement(page.jivoClose2).isDisplayed()){
            click(page.jivoClose2);
            sleep(1000);
        }

        click(page.orderOtherServices);
        try {
            waitBy(page.orderOtherServicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        // раздел 'Интернет и ТВ' отсутствует в апартаменте
        /*click(page.internetAndTV);
        try {
            waitBy(page.internetAndTVElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Интернет и ТВ' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);*/

        click(page.paymentHistory);
        try {
            waitBy(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        click(page.consumptionHistory);
        try {
            waitBy(page.consumptionHistoryElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'История потребления' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        scrollDown();

        click(page.residencePassport);
        try {
            waitBy(page.residencePassportElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Паспорт жилья' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        scrollDown();

        click(page.conditionsAndContract);
        try {
            waitBy(page.conditionsAndContractElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Условия и договор' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        scrollDown();

        click(page.newsAndNotices);
        try {
            waitBy(page.newsAndNoticesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Новости и уведомления' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

        scrollDown();

        click(page.orderFoodFromRestaurant);
        try {
            waitBy(page.orderFoodFromRestaurantElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Модальное окно заказа еды из ресторана не открылось");
        }
        click(page.closeFoodModal);

        click(page.invoices);
        try {
            waitBy(page.invoicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Счета к оплате' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.apart250, 20);

    }

    @Test(description = "Открыть все разделы машиноместа", priority = 1)
    public void openAllSectionsOfParking() throws InterruptedException {

        OpenAllSectionsPageCabinet page = new OpenAllSectionsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(page.parkingA70)){

                break;

            }

            click(page.nextObject);

        }

        click(page.profile);
        try {
            waitBy(page.profileElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Профиль жильца не загрузился");
        }
        driver.navigate().back();
        waitBy(page.parkingA70, 20);

        click(page.helpAndContacts);
        try {
            waitBy(page.helpAndContactsElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Помощь и контакты' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.parkingA70, 20);

        click(page.conditionsAndContract);
        try {
            waitBy(page.conditionsAndContractElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Условия и договор' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.parkingA70, 20);

        click(page.paymentHistory);
        try {
            waitBy(page.paymentHistoryElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.parkingA70, 20);

        click(page.invoices);
        try {
            waitBy(page.invoicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Счета к оплате' не загрузился");
        }
        driver.navigate().back();
        waitBy(page.parkingA70, 20);

    }

}
