package tests.orderCleaningTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderCleaningPageCabinet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OrderCleaningTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Хлебные крошки в заказе уборки")
    public void breadcrumbsOrderCleaning() throws InterruptedException {

        OrderCleaningPageCabinet page = new OrderCleaningPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderCleaning);
        try {
            findElement(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.generalCleaning);
        findElement(page.generalCleaningElement);

        click(page.cleaningDetailShow);
        try {
            waitBy(page.generalDetailInfo);
        } catch (TimeoutException ex){
            afterExeptionMessage("Детальное описание генеральной уборки не найдено");
        }

        click(page.cleaningDetailClose);
        Assert.assertFalse(isElementPresent(page.generalDetailInfo));

        click(page.servicesBreadcrumbs);
        findElement(page.orderCleaningElement);

        click(page.maintainingCleaning);
        findElement(page.maintainingCleaningElement);

        click(page.cleaningDetailShow);
        try {
            waitBy(page.maintainingDetailInfo);
        } catch (TimeoutException ex){
            afterExeptionMessage("Детальное описание поддерживающей уборки не найдено");
        }

        Assert.assertFalse(isElementPresent(page.generalDetailInfo));

        click(page.cleaningDetailClose);
        Assert.assertFalse(isElementPresent(page.maintainingDetailInfo));

        click(page.servicesBreadcrumbs);
        findElement(page.orderCleaningElement);

        click(page.generalCleaning);
        findElement(page.generalCleaningElement);

        findElement(page.textareaComments).sendKeys("Пожелание");

        click(page.servicesBreadcrumbs);
        findElement(page.orderCleaningElement);

        click(page.orderBreadcrumbs);
        String comment = driver.findElement(By.xpath("//textarea")).getAttribute("value");
        Assert.assertTrue(comment.equals("Пожелание"));

    }

    @Test(description = "Заказ одной услуги по уборке", priority = 1)
    public void orderOneCleaningService() throws InterruptedException {

        OrderCleaningPageCabinet page = new OrderCleaningPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderCleaning);
        try {
            findElement(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.generalCleaning);
        findElement(page.generalCleaningElement);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        findElement(page.textareaComments).sendKeys("Пожелание - заказ одной услуги по уборке");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedGeneralCleaning);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedGeneralCleaning);
        try {
            findElement(By.xpath("//p[text()='Пожелание - заказ одной услуги по уборке']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Test(description = "Заказ нескольких услуг по уборке", priority = 2)
    public void orderFewCleaningServices() throws InterruptedException {

        OrderCleaningPageCabinet page = new OrderCleaningPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderCleaning);
        try {
            findElement(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.maintainingCleaning);
        findElement(page.maintainingCleaningElement);

        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']/following-sibling::div")));
        driver.findElements(By.xpath("//input[@type='checkbox']/following-sibling::div")).stream().parallel().forEach(WebElement::click);

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxes){
            Assert.assertTrue(a.isSelected());
        }

        click(page.checkboxWashDresserPlus);
        findElement(By.xpath("//span[contains(text(),'2')]"));
        sleep(100);
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'2')]")));
        click(page.checkboxWashDresserPlus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        sleep(100);
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'3')]")));
        click(page.checkboxWashDresserPlus);
        findElement(By.xpath("//span[contains(text(),'4')]"));
        sleep(100);
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'4')]")));
        click(page.checkboxWashDresserMinus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'3')]")));

        findElement(page.choiseDateAndTimeElement);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        findElement(page.textareaComments).sendKeys("Пожелание - заказ нескольких услуг по уборке");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedMaintainingCleaning);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedMaintainingCleaning);
        try {
            findElement(By.xpath("//p[text()='Пожелание - заказ нескольких услуг по уборке']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

}
