package tests.orderFoodFromRestaurantTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderFoodFromRestaurantPageCabinet;
import java.util.concurrent.TimeUnit;

public class OrderFoodFromRestaurantTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Хлебные крошки в заказе еды")
    public void breadcrumbsOrderFood() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.apart250)) {
            click(auth.nextObject);
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.deliverToApartment);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        click(page.breadcrumbsCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.breadcrumbsMenu);
        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.breadcrumbsMainMenu);
        try {
            waitBy(auth.apart250);
        } catch (TimeoutException ex){
            afterExeptionMessage("Главная страница не загрузилась");
        }

    }

    @Test(description = "Добавление товаров в корзину", priority = 1)
    public void addFoodToBasket() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.apart250)) {
            click(auth.nextObject);
        }

        // Заказ еды из ресторана
        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        // Доставка в апартамент
        click(page.deliverToApartment);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        // Выбор чая
        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        try {
            findElement(page.barTeaDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Открыть дроп-даун меню
        click(page.barTeaDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.teaMoreHoney);
        WebElement checkboxHoney = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));
        Assert.assertTrue(checkboxHoney.isSelected());

        click(page.teaMoreLemon);
        WebElement checkboxLemon = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));
        Assert.assertTrue(checkboxLemon.isSelected());

        // Закрыть дроп-даун меню
        click(page.barTeaDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        // Выбор чая
        click(page.barCoffee);
        try {
            findElement(page.barCoffeeDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Открыть дроп-даун меню
        click(page.barCoffeeDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.coffeeMoreCoco);
        WebElement checkboxCoco = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));
        Assert.assertTrue(checkboxCoco.isSelected());

        click(page.coffeeMoreVanilla);
        WebElement checkboxVanilla = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));
        Assert.assertTrue(checkboxVanilla.isSelected());

        // Закрыть дроп-даун меню
        click(page.barCoffeeDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        // Выбор сока
        click(page.barJuice);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        // Переход в меню кухни
        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        // Выбор супа
        click(page.kitchenSoup);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        // Выбор бургера
        click(page.kitchenBurger);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        // Сумма в хлебной крошке
        String sumBreadcrumbs = driver.findElement(page.breadcrumbsCart).getText();

        // Переход в корзину
        click(page.submitToCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        // Сумма в корзине
        String sumCart = driver.findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        Assert.assertTrue(sumBreadcrumbs.contains(sumCart));

        // Добавление второго чая
        click(page.teaInCart);
        try {
            findElement(page.teaCartDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Новая сумма в корзине
        String newSumCart1 = driver.findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        Assert.assertFalse(newSumCart1.contains(sumCart));

        // Открыть дроп-даун меню
        click(page.teaCartDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        try {
            findElement(page.teaMoreTeapot1Element);
            findElement(page.teaMoreTeapot2Element);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заголовки добавок к чаю не найдены");
        }

        // Закрыть дроп-даун меню
        click(page.teaCartDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        // Добавление второго кофе
        click(page.coffeeInCart);
        try {
            findElement(page.coffeeCartDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        // Новая сумма в корзине
        String newSumCart2 = driver.findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        Assert.assertFalse(newSumCart2.contains(newSumCart1));

        // Открыть дроп-даун меню
        click(page.coffeeCartDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        try {
            findElement(page.coffeeMoreCup1Element);
            findElement(page.coffeeMoreCup2Element);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заголовки добавок к кофе не найдены");
        }

        // Закрыть дроп-даун меню
        click(page.coffeeCartDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        // Добавление второго бургера
        click(page.burgerInCart);
        try {
            findElement(page.burgerMoreElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Второй бургер не добавлен");
        }

        // Новая сумма в корзине
        String newSumCart3 = driver.findElement(By.xpath("//h3[@class='order-title ng-binding']")).getText().
                replace("В корзине товаров на сумму", "").
                replace("рублей", "").trim();

        Assert.assertFalse(newSumCart3.contains(newSumCart1));

        // Переход в меню ресторана
        click(page.breadcrumbsMenu);
        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        // Новая сумма в хлебной крошке
        String newSumBreadcrumbs = driver.findElement(page.breadcrumbsCart).getText();

        Assert.assertTrue(newSumBreadcrumbs.contains(newSumCart3));

        // Переход в меню кухни
        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        // Переход в меню бара
        click(page.bar);
        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

    }

    @Test(description = "Доставка еды в апартамент", priority = 2)
    public void deliverFoodToApartment() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.apart250)) {
            click(auth.nextObject);
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.deliverToApartment);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        try {
            findElement(page.barTeaDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barTeaDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.teaMoreHoney);
        WebElement checkboxHoney = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));
        Assert.assertTrue(checkboxHoney.isSelected());

        click(page.teaMoreLemon);
        WebElement checkboxLemon = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));
        Assert.assertTrue(checkboxLemon.isSelected());

        click(page.barTeaDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        click(page.barCoffee);
        try {
            findElement(page.barCoffeeDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffeeDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.coffeeMoreCoco);
        WebElement checkboxCoco = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));
        Assert.assertTrue(checkboxCoco.isSelected());

        click(page.coffeeMoreVanilla);
        WebElement checkboxVanilla = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));
        Assert.assertTrue(checkboxVanilla.isSelected());

        click(page.barCoffeeDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        click(page.barJuice);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        click(page.kitchenSoup);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchenBurger);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.submitToCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.date);
        click(page.chooseDateOrTime);

        click(page.time);
        click(page.chooseDateOrTime);

        sendKeys(page.comment, "Заказ еды 'Доставка в апартамент' выполнен автоматическим тестированием функционала");

        click(page.submit);
        try {
            waitBy(page.orderAccepted);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница успеха заказа не загрузилась");
        }

        try {
            waitBy(page.managerConfirmOrder);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница успеха заказа не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.deliverToApartment);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

    }

    @Test(description = "Забронировать столик", priority = 3)
    public void reserveTable() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.apart250)) {
            click(auth.nextObject);
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.reserveTable);
        try {
            findElement(page.chooseDateAndTimeReserveTableElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.date);
        click(page.chooseDateOrTime);

        click(page.time);
        click(page.chooseDateOrTime);

        sendKeys(page.comment, "Заказ еды 'Бронь стола' выполнен автоматическим тестированием функционала");

        click(page.submitReserveTable);
        try {
            waitBy(page.managerConfirmOrder);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница успеха заказа не загрузилась");
        }

        try {
            Assert.assertFalse(isElementPresent(page.orderAccepted));
        } catch (AssertionError ex){
            afterExeptionMessage("Виден текст о принятии заказа");
        }

        click(page.submit);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.reserveTable);
        try {
            findElement(page.chooseDateAndTimeReserveTableElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

    }

    @Test(description = "Забрать еду самостоятельно", priority = 4)
    public void pickYourselfFood() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        while (!isElementPresent(auth.apart250)) {
            click(auth.nextObject);
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.pickYourself);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        try {
            findElement(page.barTeaDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barTeaDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.teaMoreHoney);
        WebElement checkboxHoney = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-2']"));
        Assert.assertTrue(checkboxHoney.isSelected());

        click(page.teaMoreLemon);
        WebElement checkboxLemon = driver.findElement(By.xpath("//input[@id='addCardCheck1-0-1']"));
        Assert.assertTrue(checkboxLemon.isSelected());

        click(page.barTeaDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        click(page.barCoffee);
        try {
            findElement(page.barCoffeeDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffeeDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.coffeeMoreCoco);
        WebElement checkboxCoco = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-2']"));
        Assert.assertTrue(checkboxCoco.isSelected());

        click(page.coffeeMoreVanilla);
        WebElement checkboxVanilla = driver.findElement(By.xpath("//input[@id='addCardCheck14-0-4']"));
        Assert.assertTrue(checkboxVanilla.isSelected());

        click(page.barCoffeeDropDown);
        try {
            Assert.assertFalse(isElementPresent(page.openDropDownMenu));
        } catch (TimeoutException ex){
            afterExeptionMessage("Дроп-даун меню не свернулось");
        }

        click(page.barJuice);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        click(page.kitchenSoup);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchenBurger);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.submitToCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.date);
        click(page.chooseDateOrTime);

        click(page.time);
        click(page.chooseDateOrTime);

        sendKeys(page.comment, "Заказ еды 'Забрать самостоятельно' выполнен автоматическим тестированием функционала");

        click(page.submit);
        try {
            waitBy(page.orderAccepted);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница успеха заказа не загрузилась");
        }

        try {
            waitBy(page.managerConfirmOrder);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница успеха заказа не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.pickYourself);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

    }

}
