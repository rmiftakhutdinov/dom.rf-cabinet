package tests.orderOtherServicesTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderOtherServicesPageCabinet;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OrderOtherServicesTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Хлебные крошки в прочих услугах")
    public void breadcrumbsOrderOtherServices() throws InterruptedException {

        OrderOtherServicesPageCabinet page = new OrderOtherServicesPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderOtherServices);
        try {
            findElement(page.orderOtherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.movingAndLoading);
        findElement(page.movingAndLoadingElement);

        click(page.smallOperations);
        findElement(page.smallOperationsElement);

        click(page.servicesBreadcrumbs);
        findElement(page.movingAndLoading);

        click(page.movingAndLoading);
        findElement(page.movingAndLoadingElement);

        click(page.largeOperations);
        findElement(page.largeOperationsElement);

        findElement(page.textareaAddress).sendKeys("Казань");
        findElement(page.textareaComments).sendKeys("Пожелание");

        scrollToElement(page.servicesBreadcrumbs);
        sleep(500);

        click(page.servicesBreadcrumbs);
        findElement(page.movingAndLoading);

        click(page.orderBreadcrumbs);
        String address = findElement(By.xpath("//div[2]//textarea")).getAttribute("value");
        Assert.assertTrue(address.equals("Казань"));
        String comment = findElement(By.xpath("//div[4]//textarea")).getAttribute("value");
        Assert.assertTrue(comment.equals("Пожелание"));

    }

    @Test(description = "Заказ одной прочей услуги", priority = 1)
    public void orderOneOtherService() throws InterruptedException {

        OrderOtherServicesPageCabinet page = new OrderOtherServicesPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderOtherServices);
        try {
            findElement(page.orderOtherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.movingAndLoading);
        findElement(page.movingAndLoadingElement);

        click(page.largeOperations);
        findElement(page.largeOperationsElement);

        findElement(page.textareaAddress).sendKeys("Казань");

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        findElement(page.textareaComments).sendKeys("Пожелание - заказ одной прочей услуги");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedLargeOperations);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedLargeOperations);

        findElement(By.xpath("//p[text()='Казань']"));
        findElement(By.xpath("//p[text()='Пожелание - заказ одной прочей услуги']"));
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Ignore // Не актуально - теперь нет доп.услуг
    @Test(description = "Заказ нескольких прочих услуг", priority = 2)
    public void orderFewOtherServices() throws InterruptedException {

        OrderOtherServicesPageCabinet page = new OrderOtherServicesPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderOtherServices);
        try {
            findElement(page.orderOtherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.movingAndLoading);
        findElement(page.movingAndLoadingElement);

        click(page.smallOperations);
        findElement(page.smallOperationsElement);

        scrollToElement(page.priceElement);

        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='checkbox']/following-sibling::div")));
        driver.findElements(By.xpath("//input[@type='checkbox']/following-sibling::div")).stream().parallel().forEach(WebElement::click);

        List<WebElement> checkboxes = driver.findElements(By.xpath("//input[@type='checkbox']"));
        for (WebElement a : checkboxes){
            Assert.assertTrue(a.isSelected());
        }

        click(page.checkboxScotchPlus);
        findElement(By.xpath("//span[contains(text(),'2')]"));
        sleep(100);
        click(page.checkboxScotchPlus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        sleep(100);
        click(page.checkboxScotchPlus);
        findElement(By.xpath("//span[contains(text(),'4')]"));
        sleep(100);
        click(page.checkboxScotchMinus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'3')]")));

        findElement(page.textareaAddress).sendKeys("Казань");

        scrollToElement(page.inputDate);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        findElement(page.textareaComments).sendKeys("Пожелание - заказ нескольких прочих услуг");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedSmallOperations);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedSmallOperations);

        findElement(By.xpath("//p[text()='Казань']"));
        findElement(By.xpath("//p[text()='Пожелание - заказ нескольких прочих услуг']"));
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Test(description = "Заказ доставки воды", priority = 3)
    public void orderWaterDelivery() throws InterruptedException {

        OrderOtherServicesPageCabinet page = new OrderOtherServicesPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderOtherServices);
        try {
            findElement(page.orderOtherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Прочие услуги' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.waterDelivery);
        try {
            findElement(page.waterDeliveryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора услуги по доставке воды не загрузилась");
        }

        click(page.artesianWater);
        try {
            findElement(page.artesianWaterElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        try {
            findElement(page.waterInfo);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Описание заказа воды не найдено");
        }

        click(page.plus);
        findElement(By.xpath("//span[contains(text(),'2')]"));
        sleep(100);
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'2')]")));
        click(page.plus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        sleep(100);
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'3')]")));
        click(page.minus);
        findElement(By.xpath("//span[contains(text(),'2')]"));
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'2')]")));

        findElement(page.addComments).sendKeys("Пожелание - заказ артезианской воды");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String title1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedWater);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedWater);
        try {
            findElement(By.xpath("//p[text()='Пожелание - заказ артезианской воды']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }
        String title2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(title1.contains(title2));
        } catch (AssertionError ex){
            throw new RuntimeException("Названия заказа заказа не совпали");
        }

    }

}
