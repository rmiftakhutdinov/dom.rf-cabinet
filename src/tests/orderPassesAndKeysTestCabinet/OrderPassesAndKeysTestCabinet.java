package tests.orderPassesAndKeysTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderPassesAndKeysPageCabinet;
import pageObjects.ProfilePageCabinet;
import java.util.concurrent.TimeUnit;

public class OrderPassesAndKeysTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Хлебные крошки в заказе пропусков и ключей")
    public void breadcrumbsOrderPassesAndKeys() throws InterruptedException {

        OrderPassesAndKeysPageCabinet page = new OrderPassesAndKeysPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        findElement(page.addMoreGuest);

        findElement(page.textareaNameGuest1).sendKeys("Иванов");
        click(page.servicesBreadcrumbs);
        findElement(page.orderPassesAndKeysElement);

        click(page.orderBreadcrumbs);
        String comment = driver.findElement(By.xpath("//input")).getAttribute("value");
        Assert.assertTrue(comment.equals("Иванов"));

    }

    @Test(description = "Заказ одного бесплатного пропуска", priority = 1)
    public void orderOneFreePass() throws InterruptedException {

        OrderPassesAndKeysPageCabinet page = new OrderPassesAndKeysPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        try {
            findElement(page.addMoreGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора даты визита не загрузилась");
        }

        findElement(page.textareaNameGuest1).sendKeys("Иванов");

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedOneTimeGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        scrollToElement(page.orderedOneTimeGuest);
        sleep(500);

        click(page.orderedOneTimeGuest);
        try {
            findElement(By.xpath("//p[text()='Иванов']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги ФИО гостя не отобразилось");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Test(description = "Заказ нескольких пропусков", priority = 2)
    public void orderFewPasses() throws InterruptedException {

        OrderPassesAndKeysPageCabinet page = new OrderPassesAndKeysPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        try {
            findElement(page.addMoreGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора даты визита не загрузилась");
        }

        findElement(page.textareaNameGuest1).sendKeys("Иванов");
        click(page.addMoreGuest);
        findElement(page.textareaNameGuest2).sendKeys("Петров");
        click(page.addMoreGuest);
        findElement(page.textareaNameGuest3).sendKeys("Сидоров");
        //click(page.addMoreGuest);
        //Раскомментить после исправления бага
        // Баг в том, что сейчас доп.поля ФИО обязательные, но они не должны быть обязательными

        click(page.inputDate);
        sleep(100);
        scrollToElement(page.choiseDate);
        click(page.choiseDate);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedOneTimeGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        scrollToElement(page.orderedOneTimeGuest);

        click(page.orderedOneTimeGuest);
        try {
            findElement(By.xpath("//p[text()='Иванов']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги ФИО гостя не отобразилось");
        }
        try {
            findElement(By.xpath("//p[text()='Петров']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги ФИО гостя не отобразилось");
        }
        try {
            findElement(By.xpath("//p[text()='Сидоров']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги ФИО гостя не отобразилось");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Test(description = "Заказ платного пропуска", priority = 3)
    public void orderPayPass() throws InterruptedException {

        OrderPassesAndKeysPageCabinet page = new OrderPassesAndKeysPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.payPass);
        try {
            findElement(page.payPassElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница заказа пропуска не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

        click(page.myOrders);
        try {
            findElement(page.orderedPayPass);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

    }

}
