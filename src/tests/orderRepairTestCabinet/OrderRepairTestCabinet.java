package tests.orderRepairTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderRepairPageCabinet;
import java.util.concurrent.TimeUnit;

public class OrderRepairTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Хлебные крошки в заказе ремонта")
    public void breadcrumbsOrderRepair() throws InterruptedException {

        OrderRepairPageCabinet page = new OrderRepairPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderRepair);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.sanitaryWorks);
        findElement(page.sanitaryWaterSupply);
        findElement(page.sanitaryWorksElement);

        click(page.sanitaryWaterSupply);
        findElement(page.sanitaryWaterSupplyElement);

        click(page.servicesBreadcrumbs);
        findElement(page.orderRepairElement);
        findElement(page.sanitaryWorks);

        click(page.sanitaryWorks);
        findElement(page.sanitaryWorksElement);
        findElement(page.sanitaryWaterSupply);

        click(page.sanitaryWaterSupply);
        findElement(page.sanitaryWaterSupplyElement);

        scrollToElement(page.changeToiletBowl);

        click(page.changeToiletBowl);
        findElement(page.changeToiletBowlElement);

        click(page.submit);
        findElement(page.repairServicesCart);
        findElement(page.textareaComments).sendKeys("Пожелание");

        click(page.submit);
        findElement(page.choiseDateAndTimeElement);

        click(page.orderBreadcrumbs);
        findElement(page.repairServicesCart);

        click(page.servicesBreadcrumbs);
        findElement(page.orderRepairElement);
        findElement(page.sanitaryWorks);

        click(page.orderBreadcrumbs);
        String comment = findElement(By.xpath("//textarea")).getAttribute("value");
        Assert.assertTrue(comment.equals("Пожелание"));

    }

    @Test(description = "Заказ одной услуги по ремонту", priority = 1)
    public void orderOneRepairService() throws InterruptedException {

        OrderRepairPageCabinet page = new OrderRepairPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderRepair);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.sanitaryWorks);
        try {
            findElement(page.sanitaryWorksElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Сантехнические работы' не загрузился");
        }

        click(page.sanitaryWaterSupply);
        try {
            findElement(page.sanitaryWaterSupplyElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Водоснабжение и канализация' не загрузился");
        }

        click(page.consultation);
        try {
            findElement(page.consultationElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        findElement(page.textareaComments).sendKeys("Пожелание - заказ одной услуги по ремонту");
        click(page.submit);
        findElement(page.choiseDateAndTimeElement);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedConsultation);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedConsultation);
        try {
            findElement(By.xpath("//p[text()='Пожелание - заказ одной услуги по ремонту']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

    @Test(description = "Заказ нескольких услуг по ремонту", priority = 2)
    public void orderFewRepairServices() throws InterruptedException {

        OrderRepairPageCabinet page = new OrderRepairPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.orderRepair);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать ремонт' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.sanitaryWorks);
        try {
            findElement(page.sanitaryWorksElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Сантехнические работы' не загрузился");
        }

        click(page.sanitaryWaterSupply);
        try {
            findElement(page.sanitaryWaterSupplyElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Водоснабжение и канализация' не загрузился");
        }

        scrollToElement(page.changeToiletBowl);

        click(page.changeToiletBowl);
        try {
            findElement(page.changeToiletBowlElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        click(page.changeToiletBowl);
        try {
            findElement(page.changeToiletBowlElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        click(page.repairServicesCart);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход к списку услуг для добавления новой услуги не произошел");
        }

        click(page.electricWorks);
        try {
            findElement(page.electricWorksElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Электротехнические работы' не загрузился");
        }

        scrollToElement(page.repairSwitch);

        click(page.repairSwitch);
        try {
            findElement(page.repairSwitchElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.plus);
        findElement(By.xpath("//span[contains(text(),'2')]"));
        sleep(100);
        click(page.plus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        sleep(100);
        click(page.plus);
        findElement(By.xpath("//span[contains(text(),'4')]"));
        sleep(100);
        click(page.minus);
        findElement(By.xpath("//span[contains(text(),'3')]"));
        Assert.assertTrue(isElementPresent(By.xpath("//span[contains(text(),'3')]")));

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        click(page.repairServicesCart);
        try {
            findElement(page.orderRepairElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход к списку услуг для добавления новой услуги не произошел");
        }

        click(page.carpentryWorks);
        try {
            findElement(page.carpentryWorksElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Подраздел 'Плотницкие работы' не загрузился");
        }

        click(page.fittingPeephole);
        try {
            findElement(page.fittingPeepholeElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Карточка услуги не загрузилась");
        }

        click(page.submit);
        try {
            findElement(page.repairServicesCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Список заказываемых услуг не загрузился");
        }

        click(page.deleteRepairService);
        waitForElementIsNotPresent(page.changeToiletBowl);

        findElement(page.textareaComments).sendKeys("Пожелание - заказ нескольких услуг по ремонту");
        click(page.submit);
        findElement(page.choiseDateAndTimeElement);

        click(page.inputDate);
        sleep(100);
        click(page.choiseDate);

        click(page.inputTime);
        sleep(100);
        click(page.choiseTime);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }
        String dateOrder1 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.orders.title']")).getText();

        click(page.myOrders);
        try {
            findElement(page.orderedRepair);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Заказ не появился в списке моих заказов");
        }

        click(page.orderedRepair);
        try {
            findElement(By.xpath("//p[text()='Пожелание - заказ нескольких услуг по ремонту']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("В карточке заказанной услуги комментарий не отобразился");
        }
        String dateOrder2 = findElement(By.xpath("//h1[@ng-bind-html='$ctrl.order.name']")).getText();
        try{
            Assert.assertTrue(dateOrder1.contains(dateOrder2));
        } catch (AssertionError ex){
            throw new RuntimeException("Дата и время заказа не совпали");
        }

    }

}