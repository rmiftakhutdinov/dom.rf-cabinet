package tests.paymentHistoryTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.PaymentHistoryPageCabinet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class PaymentHistoryTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Просмотр платежей в разделе 'История платежей'")
    public void viewPaymentHistory() throws InterruptedException {

        PaymentHistoryPageCabinet page = new PaymentHistoryPageCabinet();
        AuthorizationPageCabinet auth2 = new AuthorizationPageCabinet();

        waitBy(auth2.submit, 30);
        authorizationSecond();

        while (!isElementPresent(page.apart222)) {

            click(auth2.nextObject);

        }

        click(page.paymentHistory);
        try {
            findElement(page.paymentHistoryElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'История платежей' не загрузился");
        }

        click(page.rent);
        try {
            elementIsNotPresent(page.noPayments, 3);
        } catch (Exception ex){
            afterExeptionMessage("Платежи по аренде отсутствуют");
        }

        List<String> rentElements = driver.findElements(By.xpath("//div[1]/p/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toList());
        for (String rent : rentElements) {

            try {
                Assert.assertTrue(rent.contains("Аренда"));
            } catch (AssertionError ex){
                afterExeptionMessage("Платеж по аренде не найден");
            }

        }

        click(page.communal);
        try {
            elementIsNotPresent(page.noPayments, 3);
        } catch (Exception ex){
            afterExeptionMessage("Коммунальные платежи отсутствуют");
        }

        List<String> communalElements = driver.findElements(By.xpath("//div[1]/p/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toList());
        for (String communal : communalElements) {

            try {
                Assert.assertTrue(communal.contains("Коммунальные"));
            } catch (AssertionError ex){
                afterExeptionMessage("Коммунальный платеж не найден");
            }

        }

        click(page.communalDetail);
        try {
            findElement(page.communalDetailElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент детализации платежа не найден");
        }
        click(page.communalDetail);
        waitForElementIsNotPresent(page.communalDetailElement);

        click(page.services);
        try {
            elementIsNotPresent(page.noPayments, 3);
        } catch (Exception ex){
            afterExeptionMessage("Платежи по услугам отсутствуют");
        }

        List<String> servicesElements = driver.findElements(By.xpath("//div[1]/p/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toList());
        for (String service : servicesElements) {

            try {
                Assert.assertTrue(service.contains("Прочие"));
            } catch (AssertionError ex){
                afterExeptionMessage("Платеж по услугам не найден");
            }

        }

        click(page.servicesDetail);
        try {
            findElement(page.servicesDetailElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент детализации платежа не найден");
        }
        click(page.servicesDetail);
        waitForElementIsNotPresent(page.servicesDetailElement);

        click(page.others);
        try {
            elementIsNotPresent(page.noPayments, 3);
        } catch (Exception ex){
            afterExeptionMessage("Прочие платежи отсутствуют");
        }

        List<String> otherElements = driver.findElements(By.xpath("//div[1]/p/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toList());
        for (String other : otherElements) {

            try {
                Assert.assertTrue(other.contains("Прочие"));
            } catch (AssertionError ex){
                afterExeptionMessage("Платеж по прочим услугам не найден");
            }

        }

        click(page.allPayments);
        Set<String> allElements = driver.findElements(By.xpath("//div[1]/p/span")).stream().map(element -> element.getText().trim()).collect(Collectors.toSet());
        for (String deposit : allElements) {

            try {
                Assert.assertTrue(deposit.contains("Депозит"));
            } catch (AssertionError ex){
                afterExeptionMessage("Платеж по депозиту не найден");
            }

        }

    }

}
