package tests.paymentsTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.PaymentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class AddServicesInvoicesTestAdmin extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        driver.get(page.pageURLAdmin);
        authorizationInAdmin();

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Добавление ручных счетов с типом 'Услуги'")
    public void addInvoicesServiceType() {

        PaymentsPageCabinet page = new PaymentsPageCabinet();

        int i;

        for (i=0; i<3; i++){

            driver.get("https://dev.stage.domrf.orbita.center/admin/invoice-edit?id=0");
            try {
                waitBy(page.addInvoiceElement);
            } catch (NoSuchElementException ex) {
                afterExeptionMessage("Не найден элемент страницы добавления счета");
            }

            waitForElementIsNotPresent(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

            click(page.dropDownRenter);
            sendKeys(page.inputChoiseRenter, "202" + Keys.ENTER);

            waitForElementIsNotPresent(By.xpath("//form[@name='invoiceForm']/div[2]//select[@disabled='disabled']"), 30);

            selectByText(page.objectNumberSelect, "250");

            sendKeys(page.invoicePurpose, "Счет по услугам");

            selectByText(page.invoiceTypeSelect, "Заказ услуг");

            String sum = randomString("12345", 5);
            sendKeys(page.sumInvoice, sum);

            click(page.createInvoice);
            try {
                waitBy(page.toastNewInvoice);
            } catch (NoSuchElementException ex) {
                afterExeptionMessage("Не найден элемент тоста о добавлении счета");
            }

        }

    }

}
