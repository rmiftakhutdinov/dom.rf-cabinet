package tests.paymentsTestCabinet;

import framework.Configuration;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.PaymentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class PayAllInvoicesInRentTerminal extends PaymentsPageCabinet {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getFirefoxDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Удаление привязанных карт)")
    public void deleteSavedCards() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        Assert.assertTrue(findElement(page.activeRentTerminal).isDisplayed());

        click(page.oneInvoice);
        domElementIsNotVisible(page.payInvoice, 10);

        click(page.payInvoice);

        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains("card"));
        } catch (TimeoutException ex){
            afterExeptionMessage("Привязанные карты отсутствуют");
        }

        for (;;){

            deleteCard();

            if (elementIsNotVisible(page.deleteCard)){

                break;

            }

        }

    }

    @Test(description = "Оплата всех счетов новой картой без привязки", priority = 1)
    public void payAllInvoicesWithNotSaveCard() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        Assert.assertTrue(findElement(page.activeRentTerminal).isDisplayed());

        click(page.payAllInvoices);

        if (urlContains("card")){

            selectNewCard();

        }

        waitBy(page.roskapElement);

        sendKeys(page.numberCard, "4056660000164570");
        click(page.monthCard);
        sleep(500);
        click(page.chooseMonth);
        click(page.yearCard);
        sleep(500);
        click(page.chooseYear);
        sendKeys(page.codCard, "033");
        sendKeys(page.ownerCard, "Kirill Levushkin");
        sleep(500);

        click(page.pay);
        waitBy(page.paidPageElement);

        click(page.toPaymentsHistoryRoskap);
        waitBy(page.paymentsHistoryElement);

        click(page.menu);
        sleep(1000);
        click(page.invoicesMenu);
        waitBy(page.invoicesElement);
        try {
            waitBy(page.invoicesElement);
            waitBy(page.noInvoicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Счета остались неоплаченными");
        }

    }

}
