package tests.paymentsTestCabinet;

import framework.Configuration;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.PaymentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class PayAllInvoicesInServicesTerminal extends PaymentsPageCabinet {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Удаление привязанных карт)")
    public void deleteSavedCards() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        click(page.servicesInvoices);
        Assert.assertTrue(findElement(page.activeServicesTerminal).isDisplayed());

        click(page.oneInvoice);
        domElementIsNotVisible(page.payInvoice, 10);

        click(page.payInvoice);
        try {
            new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains("card"));
        } catch (TimeoutException ex){
            afterExeptionMessage("Привязанные карты отсутствуют");
        }

        for (;;){

            deleteCard();

            if (elementIsNotVisible(page.deleteCard)){

                break;

            }

        }

    }

    @Test(description = "Оплата всех счетов новой картой без привязки", priority = 1)
    public void payAllInvoicesWithNotSaveCard() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        click(page.servicesInvoices);
        Assert.assertTrue(findElement(page.activeServicesTerminal).isDisplayed());

        click(page.payAllInvoices);

        if (urlContains("card")){

            selectNewCard();

        }

        waitBy(page.paytureElement);

        sendKeys(page.inputCardNumber, "4111111111100023");
        click(page.inputDateMonthCard);
        sleep(500);
        click(page.choiseDateMonthCard);
        click(page.inputDateYearCard);
        sleep(500);
        click(page.choiseDateYearCard);
        sendKeys(page.cvcCodCard, "123");
        sendKeys(page.ownerNameCard, "test test");
        sleep(500);

        click(page.payInvoice);
        waitBy(page.paidPageElement);

        click(page.toInvoices);
        try {
            waitBy(page.invoicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Не найден элемент раздела 'Счета к оплате'");
        }

        click(page.servicesInvoices);
        try {
            waitBy(page.noInvoicesElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Счета остались неоплаченными");
        }

    }

}
