package tests.paymentsTestCabinet;

import framework.Configuration;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.PaymentsPageCabinet;
import java.util.concurrent.TimeUnit;

public class PayOneInvoiceInServicesTerminal extends PaymentsPageCabinet {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Оплата одного счета невалидной картой")
    public void payOneInvoiceWithInvalidCard() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        click(page.servicesInvoices);
        Assert.assertTrue(findElement(page.activeServicesTerminal).isDisplayed());

        click(page.oneInvoice);
        domElementIsNotVisible(page.payInvoice, 10);

        click(page.payInvoice);

        if (urlContains("card")){

            selectNewCard();

        }

        waitBy(page.paytureElement);

        sendKeys(page.inputCardNumber, "4111111111100023");
        click(page.inputDateMonthCard);
        sleep(500);
        click(page.choiseDateMonthCard);
        click(page.inputDateYearCard);
        sleep(500);
        click(page.choiseDateYearCard);
        sendKeys(page.cvcCodCard, "555");
        sendKeys(page.ownerNameCard, "owner");
        sleep(500);

        click(page.payInvoice);
        waitBy(page.errorPayElement);

        click(page.backToPayForm);
        waitBy(page.paytureElement);

        click(page.payInvoice);
        waitBy(page.errorPayElement);

        click(page.backToPayForm);
        waitBy(page.paytureElement);

        click(page.payInvoice);
        waitBy(page.errorPayElement);

        click(page.backToPayForm);
        waitBy(page.paytureElement);

        click(page.payInvoice);
        waitBy(page.errorPayElement);

        click(page.backToPayForm);
        waitBy(page.paytureElement);

        click(page.payInvoice);
        waitBy(page.notPaidElement);

        click(page.toInvoices);
        waitBy(page.invoicesElement);

    }

    @Test(description = "Оплата одного счета с привязкой карты (с удалением привязанных карт)", priority = 1)
    public void payOneInvoiceToSaveCardWithDeleteSavedCards() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        click(page.servicesInvoices);
        Assert.assertTrue(findElement(page.activeServicesTerminal).isDisplayed());

        click(page.oneInvoice);
        domElementIsNotVisible(page.payInvoice, 10);

        click(page.payInvoice);

        if (urlContains("card")){

            for (;;){

                deleteCard();

                if (elementIsNotVisible(page.deleteCard)){

                    click(page.payInvoice);
                    break;

                }

            }

        }

        waitBy(page.paytureElement);

        sendKeys(page.inputCardNumber, "4111111111100023");
        click(page.inputDateMonthCard);
        sleep(500);
        click(page.choiseDateMonthCard);
        click(page.inputDateYearCard);
        sleep(500);
        click(page.choiseDateYearCard);
        sendKeys(page.cvcCodCard, "123");
        sendKeys(page.ownerNameCard, "test test");
        sleep(500);

        click(page.saveDataCard);
        WebElement checkbox = driver.findElement(By.xpath("//input[@id='addCardCheck']"));
        Assert.assertTrue(checkbox.isSelected());

        click(page.payInvoice);
        waitBy(page.paidPageElement);

        click(page.toPaymentsHistoryPayture);
        waitBy(page.paymentsHistoryElement);

    }

    @Test(description = "Оплата одного счета привязанной картой", priority = 2)
    public void payOneInvoiceWithSavedCard() throws InterruptedException {

        PaymentsPageCabinet page = new PaymentsPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.toInvoicesPage);
        waitBy(page.invoicesElement);
        click(page.servicesInvoices);
        Assert.assertTrue(findElement(page.activeServicesTerminal).isDisplayed());

        click(page.oneInvoice);
        domElementIsNotVisible(page.payInvoice, 10);

        click(page.payInvoice);
        try {
            urlContains("card");
        } catch (Exception ex){
            afterExeptionMessage("Переход на страницу выбора карты не произошел");
        }

        click(page.payInvoice);
        try {
            waitBy(page.paytureElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент формы оплаты 'Payture' не найден");
        }

        sendKeys(page.cvcCodCard, "123");
        sleep(500);

        click(page.payInvoice);
        try {
            waitBy(page.paidPageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы успешной оплаты не найден");
        }

        click(page.toPaymentsHistoryPayture);
        try {
            waitBy(page.paymentsHistoryElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Элемент страницы успешной оплаты не найден");
        }

    }

}
