package tests.profileTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.ProfilePageCabinet;
import java.util.concurrent.TimeUnit;

public class ProfileTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Смена эл.почты и телефона")
    public void changeEmailAndPhoneNumber() throws InterruptedException {

        ProfilePageCabinet page = new ProfilePageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        click(page.profile);
        scrollToElement(page.profileElement);
        waitBy(page.profileElement, 30);

        sendKeys(page.email, "test@test.test");
        sendKeys(page.phone, "8888888888");
        scrollToElement(page.save);
        click(page.save);

        waitForElementIsNotPresent(page.profileElement, 30);

        findElement(page.profile).click();
        waitBy(page.profileElement, 30);

        Assert.assertTrue(findElement(page.email).getAttribute("value").equals("test@test.test"));
        Assert.assertTrue(findElement(page.phone).getAttribute("value").equals("+7 888 888-88-88"));

        sendKeys(page.email, "rm@cometrica.ru");
        sendKeys(page.phone, " 927 240 11 45");
        scrollToElement(page.save);
        click(page.save);

        waitForElementIsNotPresent(page.profileElement, 30);

        findElement(page.profile).click();
        waitBy(page.profileElement, 30);

        Assert.assertTrue(findElement(page.email).getAttribute("value").equals("rm@cometrica.ru"));
        Assert.assertTrue(findElement(page.phone).getAttribute("value").equals("+7 927 240-11-45"));

    }

    @Test(description = "Смена пароля", priority = 1)
    public void changePassword() throws InterruptedException {

        ProfilePageCabinet page = new ProfilePageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        click(page.profile);
        scrollToElement(page.profileElement);
        waitBy(page.profileElement, 30);

        sendKeys(page.oldPass, "test2015");
        Assert.assertTrue(findElement(page.oldPass).getAttribute("type").equals("password"));
        click(page.eyeOld);
        Assert.assertTrue(findElement(page.oldPass).getAttribute("type").equals("text"));
        sendKeys(page.newPass, "666666");
        Assert.assertTrue(findElement(page.newPass).getAttribute("type").equals("password"));
        click(page.eyeNew);
        Assert.assertTrue(findElement(page.newPass).getAttribute("type").equals("text"));
        scrollToElement(page.save);
        click(page.save);

        waitForElementIsNotPresent(page.profileElement, 30);

        findElement(page.profile).click();
        waitBy(page.profileElement, 30);

        sendKeys(page.oldPass, "666666");
        sendKeys(page.newPass, "test2015");
        scrollToElement(page.save);
        click(page.save);

        waitForElementIsNotPresent(page.profileElement, 30);

        findElement(auth.cabinetElement);

    }

    @Test(description = "Смена языка", priority = 2)
    public void changeLanguage() throws InterruptedException {

        ProfilePageCabinet page = new ProfilePageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        click(page.profile);
        scrollToElement(page.profileElement);
        waitBy(page.profileElement, 30);

        click(page.changeToEnglish);
        try {
            findElement(page.engElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на английский язык не сработал");
        }

        click(page.changeToRussian);
        try {
            findElement(page.rusElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Перевод на русский язык не сработал");
        }

    }

    @Test(description = "Выход из авторизации", priority = 3)
    public void logout() throws InterruptedException {

        ProfilePageCabinet page = new ProfilePageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        click(page.profile);
        scrollToElement(page.profileElement);
        waitBy(page.profileElement, 30);

        click(page.logout);
        waitBy(page.authFormElement, 30);

    }

}