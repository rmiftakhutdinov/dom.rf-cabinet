package tests.promosFromPartnersTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.PromosFromPartnersPageCabinet;
import java.util.concurrent.TimeUnit;

public class PromosFromPartnersTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Открыть акцию в разделе 'Акции от партнеров'")
    public void openPromo() throws InterruptedException {

        PromosFromPartnersPageCabinet page = new PromosFromPartnersPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.promosFromPartners);
        try {
            findElement(page.promosFromPartnersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Акции от партнеров' не загрузился");
        }

        click(page.promo);
        try {
            findElement(page.promoElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход на акцию не осуществился");
        }

        click(page.breadcrumbsPromosOfPartners);
        try {
            findElement(page.promosFromPartnersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Акции от партнеров' не загрузился");
        }

    }

}
