package tests.rentObjectsTestAdmin;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.RentObjectsPageAdmin;
import java.util.concurrent.TimeUnit;

public class LeaseParkingTestAdmin extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        RentObjectsPageAdmin page = new RentObjectsPageAdmin();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Приём/сдача машиноместа №А70")
    public void rentParking() throws InterruptedException {

        RentObjectsPageAdmin page = new RentObjectsPageAdmin();

        waitBy(page.submit, 30);
        authorizationInAdmin();

        waitBy(page.objectNumber);

        selectByValue(page.changeObjectType, "string:parkings");

        waitBy(page.objectNumber);

        findElement(page.filterInput).sendKeys("A70" + Keys.ENTER);

        waitBy(page.parkingA70, 60);

        click(page.parkingA70);
        try{
            findElement(page.contracts);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Машиноместо не загрузилось");
        }

        if (elementIsNotPresent(page.labelStatusFreeParking)){

            click(page.contracts);
            click(page.dropDownMenu);
            click(page.finishContract);

            sleep(1000);

            findElement(page.finishContractConfirm).isDisplayed();
            click(page.leaveAccess);
            Assert.assertFalse(driver.findElement(page.leaveAccess).isSelected());
            click(page.finishContractConfirm);

            waitBy(page.changeStatus);

        }

        Assert.assertTrue(isElementPresent(page.labelStatusFreeParking));
        click(page.changeStatus);
        click(page.statusRented);
        click(page.save);
        waitBy(page.leaseElement, 60);

        waitForElementIsNotPresent(By.xpath("//div[@class='col-sm-8 ng-scope']/span/i"),30);

        click(page.dropDownRenter);
        findElement(page.inputChoiseRenter).sendKeys("202" + Keys.ENTER);
        Assert.assertTrue(findElement(page.renterInput202).isDisplayed());

        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(By.xpath("//div[@id='schedule__overlay']/i"))));

        click(page.nextStep);
        try {
            findElement(page.nextStepParkingElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы подтверждения сдачи м/м не найден");
        }

        click(page.lease);

        try {
            findElement(page.rentModalConfirm);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна подтверждения заселения не найден");
        }

        click(page.rentConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedParking)));
        click(page.contracts);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//span[contains(text(),'Действующий')]")));

    }

}
