package tests.rentParkingTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.RentParkingPageCabinet;
import java.util.concurrent.TimeUnit;

public class RentParkingTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Перейти в раздел 'Прочие услуги' из раздела 'Аренда машиноместа'")
    public void openOtherServicesFromRentParking() throws InterruptedException {

        RentParkingPageCabinet page = new RentParkingPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        for (;;){

            if (isElementPresent(auth.apart250)){

                break;

            }

            click(auth.nextObject);

        }

        click(page.rentParking);
        try {
            findElement(page.rentParkingElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Аренда машиноместа' не загрузился");
        }

        click(page.breadcrumbsServices);
        try {
            findElement(page.otherServicesElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Переход в раздел 'Прочие услуги' не осуществился");
        }

    }

}
