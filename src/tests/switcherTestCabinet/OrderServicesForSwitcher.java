package tests.switcherTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderCleaningPageCabinet;
import pageObjects.OrderPassesAndKeysPageCabinet;
import java.util.concurrent.TimeUnit;

public class OrderServicesForSwitcher extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Заказ одной услуги по уборке")
    public void orderOneCleaningService() throws InterruptedException {

        OrderCleaningPageCabinet page = new OrderCleaningPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorizationFourth();

        waitBy(auth.apart220);

        click(page.orderCleaning);
        try {
            findElement(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.generalCleaning);
        findElement(page.generalCleaningElement);

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

    }

    @Test(description = "Заказ одного бесплатного пропуска", priority = 1)
    public void orderOneFreePass() throws InterruptedException {

        OrderPassesAndKeysPageCabinet page = new OrderPassesAndKeysPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(page.submit, 30);
        authorizationFourth();

        waitBy(auth.apart220);

        click(page.orderPassesAndKeys);
        try {
            findElement(page.orderPassesAndKeysElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Пропуска и ключи' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.oneTimePass);
        try {
            findElement(page.addMoreGuest);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница выбора даты визита не загрузилась");
        }

        findElement(page.textareaNameGuest1).sendKeys("Иванов");

        click(page.submit);
        try {
            findElement(page.serviceOrderedElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Не найден элемент экрана успешного заказа");
        }

    }

}
