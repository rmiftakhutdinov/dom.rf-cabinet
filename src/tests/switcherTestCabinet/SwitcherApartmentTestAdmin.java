package tests.switcherTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.RentObjectsPageAdmin;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

public class SwitcherApartmentTestAdmin extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        RentObjectsPageAdmin page = new RentObjectsPageAdmin();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Заселение в апартамент №220 - наблюдатель")
    public void rentApartmentForSwitcher() throws InterruptedException, AWTException {

        RentObjectsPageAdmin page = new RentObjectsPageAdmin();

        waitBy(page.submit, 30);
        authorizationInAdmin();

        waitBy(page.objectNumber);
        findElement(page.filterInput).sendKeys("220" + Keys.ENTER);

        waitBy(page.apart220, 30);

        click(page.apart220);
        try{
            waitBy(page.contracts);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Апартамент не загрузился");
        }

        if (elementIsNotPresent(page.labelStatusFreeApartment)){

            click(page.contracts);
            click(page.dropDownMenu);
            click(page.finishContract);

            sleep(1000);

            if(findElement(page.finishContractConfirm).isDisplayed()){

                click(page.leaveAccess);
                Assert.assertFalse(driver.findElement(page.leaveAccess).isSelected());
                click(page.finishContractConfirm);

            } else {

                waitBy(page.settleModal);
                click(page.checkboxCall);
                click(page.leaveAccessUS);
                Assert.assertFalse(driver.findElement(page.leaveAccessUS).isSelected());
                Assert.assertTrue(driver.findElement(page.checkboxCall).isSelected());
                //new WebDriverWait(driver, 30).until(ExpectedConditions.elementSelectionStateToBe(By.xpath("//input[@type='checkbox']"), true));
                click(page.ejectConfirm);

            }

            waitBy(page.changeStatus);

        }

        waitBy(page.labelStatusFreeApartment);
        click(page.changeStatus);
        click(page.statusRented);
        click(page.save);
        waitBy(page.settlementElement, 60);

        waitForElementIsNotPresent(By.xpath("//div[@class='col-sm-8 ng-scope']/span/i"),30);

        click(page.dropDownRenter);
        findElement(page.inputChoiseRenter).sendKeys("306" + Keys.ENTER);
        Assert.assertTrue(findElement(page.renterInput306).isDisplayed());
        click(page.calendar);
        click(page.nextMonth);
        click(page.setDate);

        new WebDriverWait(driver, 30).until(ExpectedConditions.invisibilityOf(findElement(By.xpath("//div[@id='schedule__overlay']/i"))));

        scrollToElement(page.nextStep);
        click(page.inputIndication);
        findElement(page.indication).click();
        sleep(500);
        findElement(page.indication).click();
        sleep(500);
        findElement(page.indication).click();

        String jpg = System.getProperty("user.dir") + "\\files\\111.jpg";
        setClipboardData(jpg);
        click(page.addPhoto);

        Robot robotJPG = new Robot();
        robotJPG.delay(250);
        robotJPG.keyPress(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_V);
        robotJPG.keyRelease(KeyEvent.VK_CONTROL);
        robotJPG.keyPress(KeyEvent.VK_ENTER);
        robotJPG.keyRelease(KeyEvent.VK_ENTER);

        sleep(1000);

        waitForElementIsNotPresent(By.xpath("//button[contains(@class,'disabled')]"));

        click(page.nextStep);
        try {
            findElement(page.nextStepApartmentElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент страницы подтверждения заселения не найден");
        }

        click(page.settle);
        try {
            findElement(page.settleModalConfirm);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна подтверждения заселения не найден");
        }

        click(page.settleConfirm);

        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(findElement(page.labelStatusRentedApartment)));
        click(page.contracts);
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//span[contains(text(),'Действующий')]")));

    }

}
