package tests.switcherTestCabinet;

import framework.Configuration;
import framework.Helper;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;
import pageObjects.AuthorizationPageCabinet;
import pageObjects.OrderFoodFromRestaurantPageCabinet;
import pageObjects.SwitcherPageCabinet;
import java.util.concurrent.TimeUnit;

public class SwitcherTestCabinet extends Helper {

    @BeforeMethod
    public void setUp() throws InterruptedException {

        driver = getFirefoxDriver(Configuration.os);
        driver.manage().window().maximize();

        driver.manage().timeouts().pageLoadTimeout(30000, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().setScriptTimeout(30000, TimeUnit.MILLISECONDS);

        AuthorizationPageCabinet page = new AuthorizationPageCabinet();
        driver.get(page.pageURL);

    }

    @AfterMethod
    public void tearDown(){

        driver.quit();

    }

    @Test(description = "Проверка оплаты счета")
    public void switcherPayInvoicesTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.payments);

        findElement(page.payAll).getAttribute("class").contains("disabled");
        click(page.payAll);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тест валится из-за бага - вместо перехода на форму заполнения данных карты, должен появится тост об ошибке 403");
            //throw new RuntimeException("Тост об ошибке 403 не появился"); - раскомментить после исправления бага
        }
        click(page.closeErrorToast);

        sleep(1000);

        click(page.admin);
        click(page.backToMyAccount);
        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

    }

    @Test(description = "Проверка заказа услуги", priority = 1)
    public void switcherOrderServiceTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.orderCleaning);
        try {
            findElement(page.orderCleaningElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Раздел 'Заказать уборку' не загрузился");
        }

        waitForElementIsNotPresent(By.xpath("//div[@class='app-loader ng-scope']"), 10);

        click(page.maintainingCleaning);
        findElement(page.maintainingCleaningElement);

        click(page.submit);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка переноса даты и времени заказанной услуги", priority = 2)
    public void switcherChangeDateAndTimeServiceTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.myOrders);
        try {
            findElement(page.myOrdersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент раздела 'Мои заказы' не найден");
        }

        click(page.generalCleaning);

        click(page.changeOrder);
        findElement(page.choiseAnotherDateAndTimeElement);

        click(page.changeOrder);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка отмены заказанной услуги", priority = 3)
    public void switcherCancelServiceTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.myOrders);
        try {
            findElement(page.myOrdersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент раздела 'Мои заказы' не найден");
        }

        click(page.generalCleaning);

        click(page.cancelOrder);
        findElement(page.cancelOrderConfirm);

        click(page.cancelOrderConfirm);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка оценки заказанной услуги", priority = 4)
    public void switcherVoteServiceTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.myOrders);
        try {
            findElement(page.myOrdersElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент раздела 'Мои заказы' не найден");
        }

        click(page.voteOrder);
        findElement(page.votePopUpElement);

        click(page.choiseStar);
        click(page.send);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Ignore // Тест не актуален, функционал переноса даты выезда убрали
    @Test(description = "Проверка переноса даты выезда", priority = 5)
    public void switcherChangeDateEjectmentTest() throws InterruptedException {

        SwitcherPageCabinet page = new SwitcherPageCabinet();
        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(page.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(page.admin);
        click(page.changeUser);
        Assert.assertTrue(findElement(page.userListElement).isDisplayed());

        findElement(page.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(page.foundUserElement306)).build().perform();

        click(page.logInAsUser);
        Assert.assertTrue(findElement(page.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(page.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.conditionsAndContract);
        try {
            waitBy(page.conditionsAndContractElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Раздел 'Условия и договор' не загрузился");
        }

        click(page.informDateDeparture);
        findElement(page.dateDeparture);

        click(page.sendChangeDate);
        click(page.submit);
        try {
            waitBy(page.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка заказа доставки еды в апартамент", priority = 2)
    public void switcherDeliverFoodToApartment() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();
        SwitcherPageCabinet switcher = new SwitcherPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(switcher.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(switcher.admin);
        click(switcher.changeUser);
        Assert.assertTrue(findElement(switcher.userListElement).isDisplayed());

        findElement(switcher.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(switcher.foundUserElement306)).build().perform();

        click(switcher.logInAsUser);
        Assert.assertTrue(findElement(switcher.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(switcher.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.deliverToApartment);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        try {
            findElement(page.barTeaDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffee);
        try {
            findElement(page.barCoffeeDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffeeDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.barJuice);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        click(page.kitchenSoup);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchenBurger);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.submitToCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.submit);
        try {
            waitBy(switcher.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка заказа бронирования столика", priority = 3)
    public void switcherReserveTable() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();
        SwitcherPageCabinet switcher = new SwitcherPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(switcher.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(switcher.admin);
        click(switcher.changeUser);
        Assert.assertTrue(findElement(switcher.userListElement).isDisplayed());

        findElement(switcher.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(switcher.foundUserElement306)).build().perform();

        click(switcher.logInAsUser);
        Assert.assertTrue(findElement(switcher.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(switcher.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.reserveTable);
        try {
            findElement(page.chooseDateAndTimeReserveTableElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.submitReserveTable);
        try {
            waitBy(switcher.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    @Test(description = "Проверка заказа еды забрать самостоятельно", priority = 4)
    public void switcherPickYourselfFood() throws InterruptedException {

        AuthorizationPageCabinet auth = new AuthorizationPageCabinet();
        OrderFoodFromRestaurantPageCabinet page = new OrderFoodFromRestaurantPageCabinet();
        SwitcherPageCabinet switcher = new SwitcherPageCabinet();

        waitBy(auth.submit, 30);
        authorization();

        try {
            Assert.assertTrue(findElement(switcher.myUserName).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Мой профиль не загрузился");
        }

        click(switcher.admin);
        click(switcher.changeUser);
        Assert.assertTrue(findElement(switcher.userListElement).isDisplayed());

        findElement(switcher.search).sendKeys("Наблюдатель Арендатор");

        Actions actions = new Actions(driver);
        actions.moveToElement(findElement(switcher.foundUserElement306)).build().perform();

        click(switcher.logInAsUser);
        Assert.assertTrue(findElement(switcher.apart220).isDisplayed());
        try {
            Assert.assertTrue(findElement(switcher.userName306).isDisplayed());
        } catch (AssertionError ex) {
            throw new RuntimeException("Переключение на пользователя не сработало");
        }

        click(page.orderFood);
        try {
            findElement(page.modalFoodElement);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент модального окна не найден");
        }

        click(page.pickYourself);
        try {
            findElement(page.menu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Меню ресторана не загрузилось");
        }

        try {
            waitBy(page.menuBarActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню бара не загрузилось");
        }

        click(page.barTea);
        try {
            findElement(page.breadcrumbsCart);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент хлебной крошки 'Корзина' не найден");
        }

        try {
            findElement(page.barTeaDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffee);
        try {
            findElement(page.barCoffeeDropDown);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент кнопки дроп-даун меню не найден");
        }

        click(page.barCoffeeDropDown);
        try {
            findElement(page.openDropDownMenu);
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Элемент развернутого дроп-даун меню не найден");
        }

        click(page.barJuice);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-bar active']//div[6]/ul//li[12]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchen);
        try {
            waitBy(page.menuKitchenActive);
        } catch (TimeoutException ex){
            afterExeptionMessage("Меню кухни не загрузилось");
        }

        click(page.kitchenSoup);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[5]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.kitchenBurger);
        try {
            findElement(By.xpath("//div[@class='food-list--menu menu-kitchen active']//div[8]/ul//li[1]//div[@class='menu-category--sub-btns selected']"));
        } catch (NoSuchElementException ex){
            afterExeptionMessage("Товар не выбран");
        }

        click(page.submitToCart);
        try {
            waitBy(page.chooseDateAndTimePageElement);
        } catch (TimeoutException ex){
            afterExeptionMessage("Страница оформления заказа не загрузилась");
        }

        click(page.submit);
        try {
            waitBy(switcher.errorToast);
        } catch (TimeoutException ex) {
            throw new RuntimeException("Тост об ошибке 403 не появился");
        }

    }

    // Дописать тест на проверку перехода в карточку уведомления в разделе "Новости и уведомления" после исправления бага

}
